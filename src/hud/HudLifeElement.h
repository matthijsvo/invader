#ifndef HUDLIFEELEMENT_H_
#define HUDLIFEELEMENT_H_

/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include "HudElement.h"
#include "../framework/EventListener.h"

namespace Hud {
namespace Sfml {

/**
 * @class	HudLifeElement
 * @brief	Element of the HUD that shows the health of one or more entities
 */
class HudLifeElement : public HudElement,
						public Framework::StatChangeListener{
public:
	HudLifeElement(sf::RenderWindow& window);

	/**
	 * @brief	Resets the HudLifeElement to its original state (empty)
	 */
	void clear();

	/**
	 * @brief	Draws the HudLifeElement on the screen
	 */
	void draw();

	virtual ~HudLifeElement();
private:
	/**
	 * @brief	Change the text according to the current state of tracked entities
	 */
	void updateText();

	/**
	 * @brief	When an entity's health changes
	 */
	void notifyStatChange(const std::string& statname, double stat);
private:
	sf::Font _font;

	sf::Text _lifeText;

	std::string _lifeString;

	/**
	 * Contains all entities that need to be tracked and their health.
	 */
	std::map<std::string,int> _lifeTracker;
};

} /* namespace sfml */
} /* namespace hud */

#endif /* HUDLIFEELEMENT_H_ */

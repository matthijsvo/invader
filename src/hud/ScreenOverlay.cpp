/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include "ScreenOverlay.h"

namespace Hud {
namespace Sfml {

ScreenOverlay::ScreenOverlay(sf::RenderWindow& window) : _window(window), _duration(0.0), _stayOn(false){
	if (!_font.loadFromFile("./resources/fonts/04B_03.TTF"))
	{
		throw std::runtime_error("Font cannot be found");
	}
	_text.setFont(_font);
	_subText.setFont(_font);
	_text.setCharacterSize(window.getSize().x/10);
	_subText.setCharacterSize(window.getSize().x/30);
	setMiddle();
}

void ScreenOverlay::draw(double timeDelta) {
	if(_stayOn) {
		if(!_window.isOpen()) {
			throw std::runtime_error("Draw operation on closed window requested.");
		}
		_window.draw(_text);
		_window.draw(_subText);
	}
	else if(_duration > 0) {
		_duration -= timeDelta;
		_window.draw(_text);
		_window.draw(_subText);
		if(_duration < 0) {
			_duration = 0;
		}
	}
}

void ScreenOverlay::setText(const std::string& str, double duration, const std::string& substr) {
	_text.setString(str);
	_subText.setString(substr);
	_duration = duration;
	sf::FloatRect textRect = _text.getLocalBounds();
	_text.setOrigin(textRect.left + textRect.width/2.0f,
				   textRect.top  + textRect.height/2.0f);

	sf::FloatRect subTextRect = _subText.getLocalBounds();
	_subText.setOrigin(subTextRect.left + subTextRect.width/2.0f,
				   subTextRect.top  + subTextRect.height/2.0f);

	if(duration == -1) {
		_stayOn = true;
	}
	else {
		_stayOn = false;
	}

	if(str == "") { //substring is a small message
		setTop();
	}
	else {
		setMiddle();
	}
}

void ScreenOverlay::setMiddle() {
	if(_subText.getString() == "") {
		_text.setPosition(sf::Vector2f(_window.getSize().x/2.0f,_window.getSize().y/2.0f));
	}
	else {
		_text.setPosition(sf::Vector2f(_window.getSize().x/2.0f,(_window.getSize().y/2.0f)*0.9)); //place a bit higher
		sf::FloatRect subTextRect = _subText.getLocalBounds();
		_subText.setPosition(sf::Vector2f(_window.getSize().x/2.0f,
				(_window.getSize().y/2.0f)*1.1+subTextRect.height/2)); //and a bit lower
	}
}

void ScreenOverlay::setTop() {
	if(_subText.getString() == "") {
		_text.setPosition(sf::Vector2f(_window.getSize().x/2.0f,_window.getSize().y/10.0f));
	}
	else {
		_text.setPosition(sf::Vector2f(_window.getSize().x/2.0f,(_window.getSize().y/10.0f)*0.9)); //place a bit higher
		sf::FloatRect subTextRect = _subText.getLocalBounds();
		_subText.setPosition(sf::Vector2f(_window.getSize().x/2.0f,
				(_window.getSize().y/10.0f)*1.1+subTextRect.height/2)); //and a bit lower
	}
}

void ScreenOverlay::clear() {
	_duration = 0;
	_stayOn = false;
}

ScreenOverlay::~ScreenOverlay() {
}

} /* namespace sfml */
} /* namespace hud */

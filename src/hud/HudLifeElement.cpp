/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include "HudLifeElement.h"

namespace Hud {
namespace Sfml {

HudLifeElement::HudLifeElement(sf::RenderWindow& window) : HudElement(window) {
	if (!_font.loadFromFile("./resources/fonts/04B_03.TTF"))
	{
		throw std::runtime_error("Font cannot be found");
	}
	_lifeText.setFont(_font);
	_lifeText.setCharacterSize(20);
	_lifeText.setPosition(5,5);
}

void HudLifeElement::clear() {
	_lifeTracker.clear();
	_lifeString = "";
	_lifeText.setString(_lifeString);
}

void HudLifeElement::draw() {
	if(!_window.isOpen()) {
		throw std::runtime_error("Draw operation on closed window requested.");
	}
	_window.draw(_lifeText);
}

void HudLifeElement::updateText() {
	_lifeString.clear();
	for(const auto& entity : _lifeTracker) {
		_lifeString += entity.first;
		_lifeString += ": ";
		if(entity.second == -1) {
			_lifeString += "infinite";
		}
		else if(entity.second == 0) {
			_lifeString += "X";
		}
		else {
			for(int i = 0; i < entity.second; ++i) {
				_lifeString += "l";
			}
		}
		_lifeString += "\n";
	}
	_lifeText.setString(_lifeString);
}

void HudLifeElement::notifyStatChange(const std::string& statname, double stat) {
	auto isLiveTracked = _lifeTracker.find(statname);
	if(isLiveTracked == _lifeTracker.end()) {
		_lifeTracker.insert(std::pair<std::string,unsigned int>(statname,stat));
	}
	else {
		isLiveTracked->second = stat;
	}
	updateText();
}

HudLifeElement::~HudLifeElement() {
}

} /* namespace sfml */
} /* namespace hud */

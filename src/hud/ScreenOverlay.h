#ifndef SCREENOVERLAY_H_
#define SCREENOVERLAY_H_

/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include <SFML/Graphics.hpp>

namespace Hud {
namespace Sfml {

/**
 * @class	ScreenOverlay
 * @brief	A simple class to display a text message across the screen for a certain amount of time
 */
class ScreenOverlay {
public:
	ScreenOverlay(sf::RenderWindow& window);

	/**
	 * @brief	Draw the overlay to the screen while its timer hasn't run out
	 */
	void draw(double timeDelta);

	/**
	 * @brief	Set a certain text and/or subtest as a screen overlay for a certain duration (in seconds)
	 *
	 * If only the main string is given, it will be printed at the center of the screen, if both string and substring
	 * are given, the middle of both of them will be at the center. If only the substring is given, it is interpreted
	 * as a notification, and will be shown at the top of the screen.
	 */
	void setText(const std::string& str, double duration = -1, const std::string& substr = "");

	/**
	 * @brief	Resets the ScreenOverlay back to its original state
	 */
	void clear();

	virtual ~ScreenOverlay();
private:
	void setMiddle();
	void setTop();
private:
	sf::RenderWindow& _window;

	sf::Font _font;

	sf::Text _text;

	sf::Text _subText;

	/**
	 * How long (in seconds) the overlay should be shown on screen.
	 */
	double _duration;

	/**
	 * Whether the overlay should be shown all the time.
	 */
	bool _stayOn;
};

} /* namespace sfml */
} /* namespace hud */

#endif /* SCREENOVERLAY_H_ */

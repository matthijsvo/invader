/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include "HudElement.h"

namespace Hud {
namespace Sfml {

HudElement::HudElement(sf::RenderWindow& window) : _window(window){
}

HudElement::~HudElement() {
}

} /* namespace sfml */
} /* namespace hud */

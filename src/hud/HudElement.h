#ifndef HUDELEMENT_H_
#define HUDELEMENT_H_

/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include <SFML/Graphics.hpp>

namespace Hud {
namespace Sfml {

/**
 * @class	HudElement
 * @brief	Represents one piece of the HUD shown on screen
 */
class HudElement {
public:
	HudElement(sf::RenderWindow& window);

	/**
	 * @brief	Draws the HudElement on the screen
	 */
	virtual void draw() =0;

	virtual ~HudElement();
protected:
	sf::RenderWindow& _window;
};

} /* namespace sfml */
} /* namespace hud */

#endif /* HUDELEMENT_H_ */

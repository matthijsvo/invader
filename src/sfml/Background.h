#ifndef BACKGROUND_H_
#define BACKGROUND_H_

/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include <algorithm>
#include <SFML/Graphics.hpp>
#include "../framework/Position.h"
#include "../visual/EntitySprite.h"

namespace Visual {
namespace Sfml {

/**
 * @class	Background
 * @brief	The background of the game
 */
class Background {
public:
	Background(const std::string& location, sf::RenderWindow& window, double internalRes);

	void draw();

	virtual ~Background();
protected:
	sf::Texture _texture;

	sf::Sprite _sprite;

	sf::RenderWindow& _window;
};

} /* namespace sfml */
} /* namespace Visual */

#endif /* BACKGROUND_H_ */

/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include "AssetManager.h"

namespace Visual {
namespace Sfml {

AssetManager::AssetManager(const std::string& spriteLocation) : _spriteLocation(spriteLocation) {
}

void AssetManager::add(const std::string& name, const std::string& file, int layer) {
	sf::Texture texture;
	texture.loadFromFile(file);
	_textures.insert(std::pair<std::string, sf::Texture>(name, texture));
	_textureLayers.insert(std::pair<std::string, int>(name, layer));
}

void AssetManager::add(const std::string& name, const sf::Texture& texture, int layer) {
	_textures.insert(std::pair<std::string, sf::Texture>(name, texture));
	_textureLayers.insert(std::pair<std::string, int>(name, layer));
}

void AssetManager::add(const Framework::PropertyFile::Lookup& lookup, const std::string& spritesearchword,
		const std::string& layersearchword) {
	for(const auto& entry : lookup) { //Look at every entity in the table
		std::string filename = _spriteLocation;
		std::string name = Framework::PropertyFile::returnAsString(spritesearchword,entry.second);
		int layer = Framework::PropertyFile::returnAsInt(layersearchword,entry.second);
		filename += name;
		add(entry.first, filename, layer);
	}
}

const sf::Texture& AssetManager::get(const std::string& name) const {
	auto it = _textures.find(name);
	if(it != _textures.end()) {
		return it->second;
	}
	else {
		throw AssetException("Not able to find requested texture.");
	}
}

const int AssetManager::getLayer(const std::string& name) const {
	auto it = _textureLayers.find(name);
	if(it != _textureLayers.end()) {
		return it->second;
	}
	else {
		std::string tmp = "Not able to find requested layer data for \"" + name + "\". Check your configuration files.";
		throw AssetException(tmp);
	}
}

void AssetManager::remove(const std::string& name) {
	_textures.erase(name);
	_textureLayers.erase(name);
}

AssetManager::~AssetManager() {
}


AssetException::AssetException(std::string m)  noexcept:
			Framework::Exception("AssetException", m) {
}

AssetException::~AssetException() noexcept{
}


} /* namespace sfml */
} /* namespace Visual */

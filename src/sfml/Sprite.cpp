/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include "Sprite.h"

namespace Visual {
namespace Sfml {

Sprite::Sprite(int layer, const sf::Texture& texture, sf::RenderWindow& window, double xOffset, double yOffset,
		double scale) :
		Visual::EntitySprite(layer), _window(window), _xOffset(xOffset), _yOffset(yOffset), _scale(scale) {
	_sprite = sf::Sprite(texture);
	sf::FloatRect bounds = _sprite.getLocalBounds();
	_sprite.setOrigin(bounds.width/2, bounds.height/2); //Change to center of rotation of the sprite to its actual
														// center
}

void Sprite::draw() {
	if(!_window.isOpen()) {
		throw std::runtime_error("Draw operation on closed window requested.");
	}
	_window.draw(_sprite);
}

void Sprite::notifyPosition(const Framework::Position& position) {
	setToPosition(position);
}

void Sprite::setToPosition(const Framework::Position& position) {
	float newX = _xOffset + position.getX()/_scale;
	float newY = _yOffset + position.getY()/_scale;
	double angle = position.getRotation();
	_sprite.setPosition(newX, newY);
	_sprite.setRotation(angle+90); //SFML has different axis
}

Sprite::~Sprite() {
}

} /* namespace sfml */
} /* namespace Visual */

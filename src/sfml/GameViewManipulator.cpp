/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include "GameViewManipulator.h"

namespace Visual {
namespace Sfml {

GameViewManipulator::sPtr GameViewManipulator::make(sf::View& view, sf::View& defaultView,
		int windowX, bool& showMinimap) {
	GameViewManipulator* manipulator = new GameViewManipulator(view, defaultView, windowX, showMinimap);
	auto manipulatorSPtr = std::shared_ptr<GameViewManipulator>(manipulator);
	manipulator->addSelfPtr(manipulatorSPtr);
	return manipulatorSPtr;
}

void GameViewManipulator::clear() {
	_players = 0;
	centerView();
}

GameViewManipulator::GameViewManipulator(sf::View& view, sf::View& defaultView, int windowX, bool& showMinimap) :
		_gameView(view), _defaultView(defaultView), _windowX(windowX), _players(0), _showMinimap(showMinimap) {
	centerView();
}

void GameViewManipulator::addSelfPtr(GameViewManipulator::wPtr self) {
	_this = self;
}

void GameViewManipulator::notifyCreation(Framework::Entity::wPtr entity) {
	if(entity.lock()->getType() == "PLAYERSHIP") {
		entity.lock()->addPositionListener(_this);
		++_players;
		if(_players > 1) { //When the more than one player ship is in play, pan out so all are visible
			centerView();
		}
		else if(_players == 1) {
			_gameView = _defaultView;
			_showMinimap = true;
		}
	}
}

void GameViewManipulator::notifyDeath(Framework::Entity::wPtr entity) {
	if(entity.lock()->getType() == "PLAYERSHIP") {
		entity.lock()->addPositionListener(_this);
		--_players;
		if(_players == 0) { //Pan out when no player ships are present or when the game is lost
			centerView();
		}
		else if(_players == 1) {
			_gameView = _defaultView;
			_showMinimap = true;
		}
	}
}

void GameViewManipulator::notifyPosition(const Framework::Position& position) {
	if(_players == 1){
		Framework::Position newPos = position; //First set the center to the ship's position
		newPos.move(_windowX/4.5); //Move the view a bit forward (outward) so the the player ship is in the lower part
									// of the screen

		double theta = position.getRotation(); //Match the rotation of the ship

		double nx = newPos.getX();
		double ny = newPos.getY();

		_gameView.setCenter(sf::Vector2f(nx, ny));
		_gameView.setRotation(theta+90); //Adjust the rotation because SFML works differently
	}
}

void GameViewManipulator::centerView() {
	double center = Framework::Position::getResolution() / 2;
	_gameView = _defaultView;
	_gameView.setCenter(sf::Vector2f(center, center));
	_gameView.zoom(2.0f);
	_showMinimap = false;
}

GameViewManipulator::~GameViewManipulator() {
}

} /* namespace sfml */
} /* namespace Visual */

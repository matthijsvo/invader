#ifndef SFMLENTITYSPRITE_H_
#define SFMLENTITYSPRITE_H_

/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include <algorithm>
#include <SFML/Graphics.hpp>
#include "../framework/Position.h"
#include "../visual/EntitySprite.h"

namespace Visual {
namespace Sfml {

/**
 * @class	Sprite
 * @brief	Visualisation of a single Entity in the game, drawn on the game window
 */
class Sprite : public Visual::EntitySprite {
public:
	Sprite(int layer, const sf::Texture& texture, sf::RenderWindow& window, double xOffset = 0, double yOffset = 0,
			double scale = 1);

	/**
	 * @brief	Draw this sprite on the window of the game
	 */
	void draw();

	virtual ~Sprite();
protected:
	/**
	 * @brief	When the Entity this sprite is tracking has changed location
	 */
	void notifyPosition(const Framework::Position& position);

	/**
	 * @brief	Sets this sprite to a certain position
	 */
	virtual void setToPosition(const Framework::Position& position);
private:
	sf::Sprite _sprite;
	sf::RenderWindow& _window;

	/**
	 * The offset from the x-axis the sprite is supposed to be removed from x coordinate 0
	 * Useful for minimap
	 */
	double _xOffset;

	/**
	 * The offset from the y-axis the sprite is supposed to be removed from y coordinate 0
	 * Useful for minimap
	 */
	double _yOffset;

	/**
	 * The scale of the relative coordinates of the sprite
	 * Useful for minimap
	 */
	double _scale;
};

} /* namespace sfml */
} /* namespace Visual */
#endif /* ENTITYSPRITE_H_ */

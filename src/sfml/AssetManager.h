#ifndef ASSETMANAGER_H_
#define ASSETMANAGER_H_

/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include <SFML/Graphics.hpp>
#include <map>
#include <memory>
#include "../framework/PropertyFile.h"

namespace Visual {
namespace Sfml {

/**
 * @class	AssetManager
 * @brief	Central container for all textures
 *
 * Because textures are relatively memory intensive it is more efficient to keep a container that holds them and passes
 * references with which the actual sprites are created.
 */
class AssetManager {
public:
	typedef std::shared_ptr<AssetManager> sPtr;
	typedef std::weak_ptr<AssetManager> wPtr;
public:
	AssetManager(const std::string& spritelocation = "./resources/sprites/");

	/**
	 * @brief	Load the textures of an entire lookup table generated by an entity property file into this AssetManager
	 *
	 * Looks up the entry named <spritesearchword> to find the texture location
	 */
	virtual void add(const Framework::PropertyFile::Lookup& lookup, const std::string& spritesearchword,
			const std::string& layersearchword = "layer");

	const sf::Texture& get(const std::string& name) const;

	const int getLayer(const std::string& name) const;

	void remove(const std::string& name);

	virtual ~AssetManager();
protected:
	/**
	 * @brief	Load and add a new texture from on a file
	 */
	void add(const std::string& name, const std::string& file, int layer = 1);

	/**
	 * @brief	Add an already existing texture
	 */
	void add(const std::string& name, const sf::Texture& texture, int layer = 1);
private:
	/**
	 * The standard location where all sprites are kept.
	 */
	std::string _spriteLocation;

	/**
	 * Contains all textures
	 */
	std::map<std::string, sf::Texture> _textures;

	/**
	 * Contains the layer of every texture
	 */
	std::map<std::string, int> _textureLayers;
};

/**
 * @class	AssetException
 * @brief	Simple Exception class for reporting any errors that occur during the management of assets
 */
class AssetException : public Framework::Exception {
public:
	AssetException(std::string = "") noexcept;
	virtual ~AssetException() noexcept;
};

} /* namespace sfml */
} /* namespace Visual */

#endif /* ASSETMANAGER_H_ */

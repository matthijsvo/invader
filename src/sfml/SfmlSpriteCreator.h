#ifndef SFMLSPRITECREATOR_H_
#define SFMLSPRITECREATOR_H_

/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include "../visual/AbstractSpriteCreator.h"
#include "AssetManager.h"
#include "Sprite.h"

namespace Visual {
namespace Sfml {

/**
 * @class	SfmlSpriteCreator
 * @brief	Creates sprites for entities in game
 */
class SfmlSpriteCreator : public Visual::AbstractSpriteCreator{
public:
	SfmlSpriteCreator(AssetManager::wPtr assetManager, sf::RenderWindow& window,
			double xOffset = 0, double yOffset = 0, double scale = 1);

	EntitySprite::sPtr createSprite(const std::string& name);

	virtual ~SfmlSpriteCreator();

protected:
	/**
	 * The AssetManager that keeps all the textures for the sprites.
	 */
	AssetManager::wPtr _assetManager;

	sf::RenderWindow& _window;

	/**
	 * The offset from the x-axis the sprite is supposed to be removed from x coordinate 0
	 * Useful for minimap
	 */
	double _xOffset;

	/**
	 * The offset from the y-axis the sprite is supposed to be removed from y coordinate 0
	 * Useful for minimap
	 */
	double _yOffset;

	/**
	 * The scale of the relative coordinates of the sprite
	 * Useful for minimap
	 */
	double _scale;
};

} /* namespace sfml */
} /* namespace Visual */
#endif /* SFMLSPRITECREATOR_H_ */

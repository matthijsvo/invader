#ifndef GAMEVIEWMANIPULATOR_H_
#define GAMEVIEWMANIPULATOR_H_

/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include <SFML/Graphics.hpp>
#include "../framework/Position.h"
#include "../framework/EventListener.h"
#include "../entities/InvaderPlayerShip.h"

namespace Visual {
namespace Sfml {

/**
 * @class	GameViewManipulator
 * @brief	Changes the view of the window of the game depending on user actions
 */
class GameViewManipulator : public Framework::CreationEventListener,
							public Framework::DeathEventListener,
							public Framework::PositionEventListener{
public:
	typedef std::shared_ptr<GameViewManipulator> sPtr;
	typedef std::weak_ptr<GameViewManipulator> wPtr;
public:
	static sPtr make(sf::View& view, sf::View& defaultView, int windowX, bool& showMinimap);

	/**
	 * @brief	Reset the GameViewManipulator to its original state
	 */
	void clear();

	virtual ~GameViewManipulator();

private:
	GameViewManipulator(sf::View& view, sf::View& defaultView, int windowX, bool& showMinimap);

	void addSelfPtr(wPtr);

	void notifyCreation(Framework::Entity::wPtr entity);

	void notifyDeath(Framework::Entity::wPtr entity);

	void notifyPosition(const Framework::Position& position);

	/**
	 * @brief	Make the center of the window also the center of the view
	 */
	void centerView();
private:
	/**
	 * View of the game's window
	 */
	sf::View& _gameView;

	/**
	 * The original view of the game window
	 */
	const sf::View& _defaultView;

	/**
	 * Width of the game window
	 */
	int _windowX;

	wPtr _this;

	/**
	 * Amount of currently living player ships
	 */
	unsigned int _players;

	/**
	 * Whether or not the minimap should be shown
	 */
	bool& _showMinimap;
};

} /* namespace sfml */
} /* namespace Visual */

#endif /* GAMEVIEWMANIPULATOR_H_ */

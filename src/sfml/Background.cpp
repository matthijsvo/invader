/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include "Background.h"

namespace Visual {
namespace Sfml {

Background::Background(const std::string& location, sf::RenderWindow& window, double internalRes) :
		_window(window) {
	_texture.loadFromFile(location);
	_sprite = sf::Sprite(_texture);
	sf::FloatRect bounds = _sprite.getLocalBounds();
	_sprite.setOrigin(bounds.width/2, bounds.height/2);
	_sprite.setPosition(internalRes/2,internalRes/2);
}

void Background::draw() {
	_window.draw(_sprite);
}

Background::~Background() {
}

} /* namespace sfml */
} /* namespace Visual */

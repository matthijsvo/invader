/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include "SfmlSpriteCreator.h"

namespace Visual {
namespace Sfml {

SfmlSpriteCreator::SfmlSpriteCreator(AssetManager::wPtr assetManager, sf::RenderWindow& window,
		double xOffset, double yOffset, double scale) :
		_assetManager(assetManager) , _window(window), _xOffset(xOffset), _yOffset(yOffset), _scale(scale){
}

Visual::EntitySprite::sPtr SfmlSpriteCreator::createSprite(const std::string& name) {
	Visual::EntitySprite* sprite = new Sprite(
			_assetManager.lock()->getLayer(name), _assetManager.lock()->get(name),_window,_xOffset,_yOffset,_scale);
	return std::shared_ptr<Visual::EntitySprite>(sprite);
}

SfmlSpriteCreator::~SfmlSpriteCreator() {
}

} /* namespace sfml */
} /* namespace Visual */

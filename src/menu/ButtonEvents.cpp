/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include "ButtonEvents.h"

namespace Menu {

ButtonSignalDispatcher::ButtonSignalDispatcher() {
}

void ButtonSignalDispatcher::addButtonSignalListener(std::weak_ptr<ButtonSignalListener> listener) {
	_listeners.insert(listener);
}

void ButtonSignalDispatcher::removeButtonSignalListener(std::weak_ptr<ButtonSignalListener> listener) {
	_listeners.erase(listener);
}

void ButtonSignalDispatcher::dispatchButtonSignal(const std::string& type, const std::string& content) {
	for(const auto& listener : _listeners) {
		listener.lock()->notifyButtonSignal(type,content);
	}
}

ButtonSignalDispatcher::~ButtonSignalDispatcher() {
}

ButtonChangeDispatcher::ButtonChangeDispatcher() {
}

void ButtonChangeDispatcher::addButtonChangeListener(std::weak_ptr<ButtonChangeListener> listener) {
	_listeners.insert(listener);
}

void ButtonChangeDispatcher::removeButtonChangeListener(std::weak_ptr<ButtonChangeListener> listener) {
	_listeners.erase(listener);
}

void ButtonChangeDispatcher::dispatchButtonChange(const std::string& type, const std::string& content) {
	for(const auto& listener : _listeners) {
		listener.lock()->notifyButtonChange(type,content);
	}
}

void ButtonChangeDispatcher::dispatchButtonChange(const std::string& type, bool state) {
	for(const auto& listener : _listeners) {
		listener.lock()->notifyButtonChange(type,state);
	}
}

ButtonChangeDispatcher::~ButtonChangeDispatcher() {
}

ButtonCreationEventDispatcher::ButtonCreationEventDispatcher() {
}

void ButtonCreationEventDispatcher::addButtonCreationListener(std::weak_ptr<ButtonCreationEventListener> listener) {
	_listeners.insert(listener);
}

void ButtonCreationEventDispatcher::removeButtonCreationListener(std::weak_ptr<ButtonCreationEventListener> listener) {
	_listeners.erase(listener);
}

void ButtonCreationEventDispatcher::dispatchButtonCreationEvent(std::weak_ptr<InvaderMenuButton> button) {
	for(const auto& listener : _listeners) {
		listener.lock()->notifyButtonCreation(button);
	}
}

ButtonCreationEventDispatcher::~ButtonCreationEventDispatcher() {
}

} /* namespace menu */

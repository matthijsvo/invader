/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include "SfmlButton.h"

namespace Menu {
namespace Sfml {

SfmlButton::SfmlButton(sf::RenderWindow& window, unsigned int x, unsigned int y, unsigned int height, const std::string& text):
		_window(window) {
	if (!_font.loadFromFile("./resources/fonts/04B_03.TTF"))
	{
		throw std::runtime_error("Font cannot be found");
	}
	_color = sf::Color(170,0,0);
	_selectedColor = sf::Color::White;
	_text = text;

	_label.setFont(_font);
	_label.setCharacterSize(height/2);

	_label.setString(_text);
	sf::FloatRect textRect = _label.getLocalBounds();
	_label.setOrigin(textRect.left + textRect.width/2.0f,
				   textRect.top  + textRect.height/2.0f);

	_label.setPosition(x,y);
	_label.setColor(_color);

}

void SfmlButton::draw() {
	_label.setFont(_font); //Don't know why this is needed here, but apparently it is
	_window.draw(_label);
}

void SfmlButton::notifyButtonChange(const std::string& type, const std::string& content) {
	if(type == "CONTENT") {
		_label.setString(content);
		sf::FloatRect textRect = _label.getLocalBounds();
		_label.setOrigin(textRect.left + textRect.width/2.0f,
					   textRect.top  + textRect.height/2.0f);
	}
}

void SfmlButton::notifyButtonChange(const std::string& type, bool state) {
	if(type == "MOUSEOVER") {
		if(state == true) {
			_label.setColor(_selectedColor);
		}
		else {
			_label.setColor(_color);
		}
	}
}

SfmlButton::~SfmlButton() {
}

} /* namespace sfml */
} /* namespace menu */

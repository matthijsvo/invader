#ifndef INVADERMENUVISUALISER_H_
#define INVADERMENUVISUALISER_H_

/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include <SFML/Graphics.hpp>
#include <vector>
#include "InvaderMenuButton.h"
#include "SfmlButton.h"

namespace Menu {
namespace Sfml {

/**
 * @class	InvaderMenuVisualiser
 * @brief	The visual representation of a menu
 */
class InvaderMenuVisualiser : public Menu::ButtonCreationEventListener{
public:
	typedef std::shared_ptr<InvaderMenuVisualiser> sPtr;
	typedef std::weak_ptr<InvaderMenuVisualiser> wPtr;
public:
	InvaderMenuVisualiser(sf::RenderWindow& window, const std::string& title = "");

	/**
	 * @brief	Draw the visuals of all buttons in a menu
	 */
	void drawAll();

	virtual ~InvaderMenuVisualiser();
private:
	/**
	 * @brief	When a button has been created, create a visual for it
	 */
	void notifyButtonCreation(std::weak_ptr<InvaderMenuButton> button);
private:
	sf::RenderWindow& _window;

	sf::Font _font;

	sf::Text _title;

	/**
	 * Contains the visuals of all buttons in a menu
	 */
	std::vector<SfmlButton::sPtr> _buttons;
};

} /* namespace sfml */
} /* namespace menu */

#endif /* INVADERMENUVISUALISER_H_ */

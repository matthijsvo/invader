#ifndef INVADERMENULEVELBUTTON_H_
#define INVADERMENULEVELBUTTON_H_

/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include "InvaderMenuButton.h"

namespace Menu {

/**
 * @class	InvaderMenuLevelButton
 * @brief	A simple button that loads a certain predefined level when clicked
 */
class InvaderMenuLevelButton: public Menu::InvaderMenuButton {
public:
	InvaderMenuLevelButton(const std::string& text, const std::string& content);

	void mouseClick(unsigned int x, unsigned int y);

	virtual void buttonPress(const std::string& key);

	virtual ~InvaderMenuLevelButton();
protected:
	std::string _content;
};

} /* namespace menu */

#endif /* INVADERMENULEVELBUTTON_H_ */

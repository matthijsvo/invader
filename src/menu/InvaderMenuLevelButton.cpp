/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include "InvaderMenuLevelButton.h"

namespace Menu {

InvaderMenuLevelButton::InvaderMenuLevelButton(const std::string& text, const std::string& content) :
		_content(content) {
	_text = text;
}

void InvaderMenuLevelButton::mouseClick(unsigned int x, unsigned int y) {
	if(_button.withinBoundaries(x,y)) {
		dispatchButtonSignal("LEVELSELECT", _content);
	}
}

void InvaderMenuLevelButton::buttonPress(const std::string& key) {
	if(_mouseOver && key == "ENTER") {
		dispatchButtonSignal("LEVELSELECT", _content);
	}
}


InvaderMenuLevelButton::~InvaderMenuLevelButton() {
}

} /* namespace menu */

/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include "InvaderMenuButton.h"

namespace Menu {

bool InvaderMenuButton::Rectangle::withinBoundaries(unsigned int x, unsigned int y) {
	return (x > _left && x < _right && y < _top && y > _bottom);
}

const unsigned int InvaderMenuButton::Rectangle::getMiddleX() {
	return (_left + _right)/2;
}

const unsigned int InvaderMenuButton::Rectangle::getMiddleY() {
	return (_top + _bottom)/2;
}

const unsigned int InvaderMenuButton::Rectangle::getWidth() {
	return (_right - _left);
}

const unsigned int InvaderMenuButton::Rectangle::getHeight() {
	return (_top - _bottom);
}

InvaderMenuButton::InvaderMenuButton() :
				 _mouseOver(false), _button(Rectangle(0,0,0,0)), _text("") {
}

const InvaderMenuButton::Rectangle& InvaderMenuButton::getRect() {
	return _button;
}

const std::string& InvaderMenuButton::getText() {
	return _text;
}

void InvaderMenuButton::setSize(unsigned int left, unsigned int right, unsigned int top, unsigned int bottom) {
	_button = Rectangle(left,right,top,bottom);
}

void InvaderMenuButton::mouseOver(unsigned int x, unsigned int y) {
	bool tmp;
	if(_button.withinBoundaries(x,y)) {
		tmp = true;
	}
	else {
		tmp = false;
	}
	if(_mouseOver != tmp) {
		_mouseOver = tmp;
		dispatchButtonChange("MOUSEOVER",_mouseOver);
	}
}

void InvaderMenuButton::mouseClick(unsigned int x, unsigned int y) {
	//Ignore
}

void InvaderMenuButton::buttonPress(const std::string& key) {
	//Ignore
}

void InvaderMenuButton::textTyped(const std::string& str) {
	//Ignore
}

InvaderMenuButton::~InvaderMenuButton() {
}

} /* namespace menu */

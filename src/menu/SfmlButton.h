#ifndef SFMLBUTTON_H_
#define SFMLBUTTON_H_

/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include "ButtonEvents.h"
#include <SFML/Graphics.hpp>
#include <memory>

namespace Menu {
namespace Sfml {

/**
 * @class	SfmlButton
 * @brief	A simple visual representation of a signle menu button
 */
class SfmlButton : public Menu::ButtonChangeListener {
public:
	typedef std::shared_ptr<SfmlButton> sPtr;
	typedef std::weak_ptr<SfmlButton> wPtr;
public:
	SfmlButton(sf::RenderWindow& window, unsigned int x, unsigned int y, unsigned int height, const std::string& text);

	/**
	 * @brief	Draw this button visual to the screen
	 */
	void draw();

	virtual ~SfmlButton();
private:
	void notifyButtonChange(const std::string& type, const std::string& content);

	void notifyButtonChange(const std::string& type, bool state);
private:
	sf::RenderWindow& _window;

	std::string _text;

	sf::Font _font;

	sf::Text _label;

	/**
	 * The color of the button when the mouse is not hovering above it.
	 */
	sf::Color _color;

	/**
	 * The color of the button when the mouse is hovering about it.
	 */
	sf::Color _selectedColor;
};

} /* namespace sfml */
} /* namespace menu */

#endif /* SFMLBUTTON_H_ */

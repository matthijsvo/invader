/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include "InvaderCustomMenuButton.h"

namespace Menu {

InvaderCustomMenuButton::InvaderCustomMenuButton(const std::string& text, const std::string& levelPath) :
			InvaderMenuLevelButton(text,""), _levelPath(levelPath),
			_acceptTyping(false) {
}

void InvaderCustomMenuButton::mouseClick(unsigned int x, unsigned int y) {
	_acceptTyping = _button.withinBoundaries(x,y);
}

void InvaderCustomMenuButton::textTyped(const std::string& str) {
	if(_acceptTyping) {
		if(str == "BACKSPACE") {
			if(_content.size() > 0) {
				_content.pop_back();
				dispatchButtonChange("CONTENT",_content);
			}
		}
		else {
			_content += str;
			dispatchButtonChange("CONTENT",_content);
		}
	}
}

void InvaderCustomMenuButton::buttonPress(const std::string& key) {
	if(_acceptTyping && key == "ENTER") {
		dispatchButtonSignal("LEVELSELECT", _levelPath + _content + ".ini");
	}
}

InvaderCustomMenuButton::~InvaderCustomMenuButton() {
}

} /* namespace menu */

#ifndef BUTTONEVENTS_H_
#define BUTTONEVENTS_H_

/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include <memory>
#include <set>

namespace Menu {

/**
 * @brief	All events to be used by buttons in a menu
 *
 * Handles buttons that are clicked, changed by the user, or created.
 */

class InvaderMenuButton;

class ButtonSignalListener {
public:
	typedef std::shared_ptr<ButtonSignalListener> sPtr;
	typedef std::weak_ptr<ButtonSignalListener> wPtr;
public:
	virtual ~ButtonSignalListener() {}

	virtual void notifyButtonSignal(const std::string& type, const std::string& content) = 0;
};

class ButtonSignalDispatcher {
public:
	typedef std::shared_ptr<ButtonSignalDispatcher> sPtr;
	typedef std::weak_ptr<ButtonSignalDispatcher> wPtr;
public:
	ButtonSignalDispatcher();
	virtual ~ButtonSignalDispatcher();
	void addButtonSignalListener(std::weak_ptr<ButtonSignalListener> listener);
	void removeButtonSignalListener(std::weak_ptr<ButtonSignalListener> listener);
protected:
	void dispatchButtonSignal(const std::string& type, const std::string& content = "");
private:
	std::set<std::weak_ptr<ButtonSignalListener>,std::owner_less<std::weak_ptr<ButtonSignalListener> > >
		_listeners;
};

class ButtonChangeListener {
public:
	typedef std::shared_ptr<ButtonChangeListener> sPtr;
	typedef std::weak_ptr<ButtonChangeListener> wPtr;
public:
	virtual ~ButtonChangeListener() {}

	virtual void notifyButtonChange(const std::string& type, const std::string& content) = 0;
	virtual void notifyButtonChange(const std::string& type, bool state) = 0;
};

class ButtonChangeDispatcher {
public:
	typedef std::shared_ptr<ButtonChangeDispatcher> sPtr;
	typedef std::weak_ptr<ButtonChangeDispatcher> wPtr;
public:
	ButtonChangeDispatcher();
	virtual ~ButtonChangeDispatcher();
	void addButtonChangeListener(std::weak_ptr<ButtonChangeListener> listener);
	void removeButtonChangeListener(std::weak_ptr<ButtonChangeListener> listener);
protected:
	void dispatchButtonChange(const std::string& type, const std::string& content);
	void dispatchButtonChange(const std::string& type, bool state);
private:
	std::set<std::weak_ptr<ButtonChangeListener>,std::owner_less<std::weak_ptr<ButtonChangeListener> > >
		_listeners;
};

class ButtonCreationEventListener {
public:
	typedef std::shared_ptr<ButtonCreationEventListener> sPtr;
	typedef std::weak_ptr<ButtonCreationEventListener> wPtr;
public:
	virtual ~ButtonCreationEventListener() {}

	virtual void notifyButtonCreation(std::weak_ptr<InvaderMenuButton> button) = 0;
};

class ButtonCreationEventDispatcher {
public:
	typedef std::shared_ptr<ButtonCreationEventDispatcher> sPtr;
	typedef std::weak_ptr<ButtonCreationEventDispatcher> wPtr;
public:
	ButtonCreationEventDispatcher();
	virtual ~ButtonCreationEventDispatcher();
	void addButtonCreationListener(std::weak_ptr<ButtonCreationEventListener> listener);
	void removeButtonCreationListener(std::weak_ptr<ButtonCreationEventListener> listener);
protected:
	void dispatchButtonCreationEvent(std::weak_ptr<InvaderMenuButton> button);
private:
	std::set<std::weak_ptr<ButtonCreationEventListener>,std::owner_less<std::weak_ptr<ButtonCreationEventListener> > > _listeners;
};

} /* namespace menu */

#endif /* BUTTONEVENTS_H_ */

/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include "InvaderMenu.h"

namespace Menu {

InvaderMenu::sPtr InvaderMenu::make(unsigned int resX, unsigned int resY, const std::string& levelDirectory) {
	// Work with raw pointer temporarily because shared_ptr can only access public methods.
	InvaderMenu* game = new InvaderMenu(resX,resY,levelDirectory);
	auto gameSPtr = std::shared_ptr<InvaderMenu>(game);
	game->setSelfPtr(gameSPtr);
	return gameSPtr;
}

void InvaderMenu::setSelfPtr(wPtr self) {
	_this = self;
}

InvaderMenu::InvaderMenu(unsigned int resX, unsigned int resY, const std::string& levelDirectory) :
	_resX(resX), _resY(resY), _levelDirectory(levelDirectory) {
}

void InvaderMenu::buildMenu(const Framework::PropertyFile::Lookup& lookup) {
	const auto foundButton = lookup.equal_range("button");
	if(foundButton.first != lookup.end()) {
		for(auto it = foundButton.first; it!=foundButton.second; ++it){
			InvaderMenuButton::sPtr button;

			std::string type = Framework::PropertyFile::returnAsString("type",it->second);
			if(type == "LEVELBUTTON") {
				std::string text = Framework::PropertyFile::returnAsString("text",it->second);
				std::string go = Framework::PropertyFile::returnAsString("goto",it->second);
				button = std::make_shared<InvaderMenuLevelButton>(InvaderMenuLevelButton(text,go));
			}
			else if(type == "CUSTOMLEVELBUTTON") {
				button = std::make_shared<InvaderCustomMenuButton>(
							InvaderCustomMenuButton("[enter level]", _levelDirectory));
			}
			else if(type == "QUITBUTTON") {
				button = std::make_shared<InvaderMenuQuitButton>(InvaderMenuQuitButton("QUIT"));
			}
			else if(type == "EMPTY") {
				button = std::make_shared<InvaderMenuButton>(InvaderMenuButton()); //Leave an empty space
			}
			else {
				throw Framework::ParseException("Unknown button type requested.");
			}

			button->addButtonSignalListener(_this);
			_buttons.push_back(button);
		}
	}
	calculateSizes();
	for(auto& button : _buttons) { //Only when all buttons are created and set on their proper position, notify of their
									// creation
		dispatchButtonCreationEvent(button);
	}
}

void InvaderMenu::clear() {
	_buttons.clear();
}

void InvaderMenu::mouseOver(unsigned int x, unsigned int y) {
	for(auto& button : _buttons) {
		button->mouseOver(x,y);
	}
}

void InvaderMenu::mouseClick(unsigned int x, unsigned int y) {
	for(auto& button : _buttons) {
		button->mouseClick(x,y);
	}
}

void InvaderMenu::buttonPress(const std::string& key) {
	for(auto& button : _buttons) {
		button->buttonPress(key);
	}
}

void InvaderMenu::textTyped(const std::string& str) {
	for(auto& button : _buttons) {
		button->textTyped(str);
	}
}

void InvaderMenu::calculateSizes() {
	unsigned int buttonAmount = _buttons.size();
	unsigned int border = 5;
	unsigned int titleBottom = (_resY/5); //Reserve space for title
	unsigned int verticalSpace = _resY - titleBottom - 5; //5 extra border at bottom
	unsigned int blockHeight = verticalSpace / buttonAmount;
	for(unsigned int i = 0; i < buttonAmount; ++i) {
		_buttons.at(i)->setSize(border, //Left
								_resX - border, //Right
								titleBottom + (i+1)*blockHeight, //Top
								titleBottom + i*blockHeight); //Bottom
	}
}

void InvaderMenu::notifyButtonSignal(const std::string& type, const std::string& content) {
	dispatchButtonSignal(type,content);
}

InvaderMenu::~InvaderMenu() {
}

} /* namespace menu */

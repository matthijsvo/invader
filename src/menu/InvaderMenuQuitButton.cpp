/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include "InvaderMenuQuitButton.h"

namespace Menu {

InvaderMenuQuitButton::InvaderMenuQuitButton(const std::string& label) {
	_text = label;
}

void InvaderMenuQuitButton::mouseClick(unsigned int x, unsigned int y) {
	if(_button.withinBoundaries(x,y)) {
		dispatchButtonSignal("QUIT");
	}
}

InvaderMenuQuitButton::~InvaderMenuQuitButton() {
}

} /* namespace menu */

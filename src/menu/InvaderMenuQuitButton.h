#ifndef INVADERMENUQUITBUTTON_H_
#define INVADERMENUQUITBUTTON_H_

/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include "InvaderMenuButton.h"

namespace Menu {

/**
 * @class	InvaderMenuQuitButton
 * @brief	A simple button that exits the game when clicked
 */
class InvaderMenuQuitButton: public Menu::InvaderMenuButton {
public:
	InvaderMenuQuitButton(const std::string& label);

	void mouseClick(unsigned int x, unsigned int y);

	virtual ~InvaderMenuQuitButton();
};

} /* namespace menu */

#endif /* INVADERMENUQUITBUTTON_H_ */

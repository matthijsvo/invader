#ifndef INVADERMENUBUTTON_H_
#define INVADERMENUBUTTON_H_

/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include "ButtonEvents.h"
#include <memory>
#include <set>

namespace Menu {

/**
 * @class	InvaderMenuButton
 * @brief	Base class for all buttons in a menu
 */
class InvaderMenuButton : public ButtonSignalDispatcher,
						public ButtonChangeDispatcher {
public:
	typedef std::shared_ptr<InvaderMenuButton> sPtr;
	typedef std::weak_ptr<InvaderMenuButton> wPtr;

	/**
	 * Object for storing the size of the button and a few simple functions
	 */
	struct Rectangle {
		unsigned int _left;
		unsigned int _right;
		unsigned int _top;
		unsigned int _bottom;
		Rectangle(unsigned int left, unsigned int right, unsigned int top, unsigned int bottom) :
				_left(left), _right(right), _top(top), _bottom(bottom) {
		}
		bool withinBoundaries(unsigned int x, unsigned int y);
		const unsigned int getMiddleX();
		const unsigned int getMiddleY();
		const unsigned int getWidth();
		const unsigned int getHeight();
	};
public:
	InvaderMenuButton();

	/**
	 * @brief	Get the rectangle in which the button responds to mouse input
	 */
	const Rectangle& getRect();

	const std::string& getText();

	void setSize(unsigned int left, unsigned int right, unsigned int top, unsigned int bottom);

	/**
	 * @brief	Check if the user has moved inside the rectangle of this button
	 */
	void mouseOver(unsigned int x, unsigned int y);

	/**
	 * @brief	Check if the user has clicked the mouse inside the rectangle of this button
	 */
	virtual void mouseClick(unsigned int x, unsigned int y);

	/**
	 * @brief	Handle the user pressing a key
	 */
	virtual void buttonPress(const std::string& key);

	/**
	 * @brief	Handle the user typing multiple characters
	 */
	virtual void textTyped(const std::string& str);

	virtual ~InvaderMenuButton();

protected:
	/**
	 * Whether the mouse pointer is currently inside the this button's Rectangle
	 */
	bool _mouseOver;

	/**
	 * The rectangular area on the screen in which the button responds to mouse input
	 */
	Rectangle _button;

	std::string _text;
};

} /* namespace menu */

#endif /* INVADERMENUBUTTON_H_ */

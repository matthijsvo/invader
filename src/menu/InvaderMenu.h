#ifndef INVADERMENU_H_
#define INVADERMENU_H_

/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include <vector>
#include "../framework/EventListener.h"
#include "../framework/EventDispatcher.h"
#include "../framework/PropertyFile.h"
#include "ButtonEvents.h"
#include "InvaderMenuLevelButton.h"
#include "InvaderCustomMenuButton.h"
#include "InvaderMenuQuitButton.h"

namespace Menu {

/**
 * @class	InvaderMenu
 * @brief	An entire menu screen
 */
class InvaderMenu : public Framework::CreationEventDispatcher,
					public ButtonSignalDispatcher,
					public ButtonSignalListener,
					public ButtonCreationEventDispatcher {
public:
	typedef std::shared_ptr<InvaderMenu> sPtr;
	typedef std::weak_ptr<InvaderMenu> wPtr;
public:
	static sPtr make(unsigned int resX, unsigned int resY, const std::string& levelDirectory);

	/**
	 * @brief	Generates a menu screen from the lookup table provided by a menu property file
	 */
	void buildMenu(const Framework::PropertyFile::Lookup& lookup);

	/**
	 * @brief	Resets this menu back to its original state (empty)
	 */
	void clear();

	/**
	 * @brief	Alerts all buttons that the mouse has moved
	 */
	void mouseOver(unsigned int x, unsigned int y);

	/**
	 * @brief	Alerts all buttons of a left mouseclick
	 */
	void mouseClick(unsigned int x, unsigned int y);

	/**
	 * @brief	Alerts all buttons that a key has been pressed
	 */
	void buttonPress(const std::string& key);

	/**
	 * @brief	Alerts all buttons that the user has typed multiple characters
	 */
	void textTyped(const std::string& str);

	virtual ~InvaderMenu();
private:
	InvaderMenu(unsigned int resX, unsigned int resY, const std::string& levelDirectory);

	void setSelfPtr(wPtr self);

	/**
	 * @brief	Calculates and sets the correct size of all buttons in the menu
	 */
	void calculateSizes();

	/**
	 * @brief	When a button has emitted a signal, pass it through to all listeners
	 */
	void notifyButtonSignal(const std::string& type, const std::string& content);
private:
	wPtr _this;

	/**
	 * Window width
	 */
	unsigned int _resX;

	/**
	 * Window height
	 */
	unsigned int _resY;

	std::string _levelDirectory;

	/**
	 * Contains all buttons of the menu
	 */
	std::vector<InvaderMenuButton::sPtr> _buttons;
};

} /* namespace menu */

#endif /* INVADERMENU_H_ */

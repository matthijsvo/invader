/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include "InvaderMenuVisualiser.h"

namespace Menu {
namespace Sfml {

InvaderMenuVisualiser::InvaderMenuVisualiser(sf::RenderWindow& window, const std::string& title) : _window(window) {
	if (!_font.loadFromFile("./resources/fonts/04B_03.TTF"))
	{
		throw std::runtime_error("Font cannot be found");
	}
	_title.setFont(_font);
	_title.setCharacterSize(window.getSize().y/5-10);
	_title.setString(title);
	sf::FloatRect textRect = _title.getLocalBounds();
	_title.setOrigin(textRect.left + textRect.width/2.0f, 0);
	_title.setPosition(window.getSize().x/2, 0);
}

void InvaderMenuVisualiser::drawAll() {
	_window.draw(_title);
	for(auto& button : _buttons) {
		if(!_window.isOpen()) {
			throw std::runtime_error("Draw operation on closed window requested.");
		}
		button->draw();
	}
}

void InvaderMenuVisualiser::notifyButtonCreation(std::weak_ptr<InvaderMenuButton> button) {
	InvaderMenuButton::Rectangle rect = button.lock()->getRect();
	auto buttonVisual = std::make_shared<SfmlButton>(
			SfmlButton(_window, rect.getMiddleX(), rect.getMiddleY(), rect.getHeight(), button.lock()->getText()));
	button.lock()->addButtonChangeListener(buttonVisual);
	_buttons.push_back(buttonVisual);
}

InvaderMenuVisualiser::~InvaderMenuVisualiser() {
}

} /* namespace sfml */
} /* namespace menu */

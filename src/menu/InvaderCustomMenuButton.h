#ifndef INVADERCUSTOMMENUBUTTON_H_
#define INVADERCUSTOMMENUBUTTON_H_

/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include "InvaderMenuLevelButton.h"

namespace Menu {

/**
 * @class	InvaderCustomMenuButton
 * @brief	Provides a button in which the user can type a level name
 */
class InvaderCustomMenuButton: public Menu::InvaderMenuLevelButton {
public:
	InvaderCustomMenuButton(const std::string& text, const std::string& levelPath);

	void mouseClick(unsigned int x, unsigned int y);

	void textTyped(const std::string& str);

	void buttonPress(const std::string& key);

	virtual ~InvaderCustomMenuButton();
private:
	std::string _levelPath;
	bool _acceptTyping;
};

} /* namespace menu */

#endif /* INVADERCUSTOMMENUBUTTON_H_ */

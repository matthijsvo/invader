#ifndef ENTITYMANAGER_H_
#define ENTITYMANAGER_H_

/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include <map>
#include <unordered_set>
#include <vector>
#include "../framework/Entity.h"
#include "../framework/EventDispatcher.h"
#include "AbstractEntityCreator.h"
#include "CollisionSolver.h"

namespace Game {

/**
 * @class	InvaderEntityManager
 * @brief	Manages and updates all contained entities
 *
 * This manager keeps all entities alive and is regarded as the sole owner of entities.
 * It can independently create entities (in response to CreationRequests) and has its own AbstractEntityCreator to do so.
 * This is the only way entities should ever be created.
 *
 * In practice, this Manager is meant to update the game logic of all entities within a certain timeframe, and check
 * for collisions (which are handled by its CollisionSolver).
 *
 * The AbstractEntityCreator and CollisionSolver are owned solely by their InvaderEntityManager.
 */
class InvaderEntityManager : public Framework::DeathEventListener,
						public Framework::CreationEventListener,
						public Framework::DeathEventDispatcher,
						public Framework::CreationEventDispatcher,
						public Framework::CreationRequestListener{
public:
	typedef std::shared_ptr<InvaderEntityManager> sPtr;
	typedef std::weak_ptr<InvaderEntityManager> wPtr;

public:
	/**
	 * @brief	Creates an InvaderEntityManager and returns a shared_ptr to it
	 *
	 * This constructor, following the Named Constructor Idiom, makes sure that the InvaderEntityManager is created as
	 * usual, and also sets a pointer to itself (see "_this").
	 */
    static sPtr make(AbstractEntityCreator::sPtr AbstractEntityCreator, CollisionSolver::sPtr collisionSolver);

	/**
	 * @brief	Resets the InvaderEntityManager to its original state
	 *
	 * Clears the InvaderEntityManager of all its entities, which are immediately destroyed.
	 * This method is meant to be used when changing levels.
	 */
	void clear();

	/**
	 * @brief	Update all entities within this InvaderEntityManager
	 *
	 * Mainly used to update game logic.
	 */
	void updateAll(double timeDelta);

	virtual ~InvaderEntityManager();

private:
	/**
	 * @brief	Constructor for InvaderEntityManager
	 *
     * Receives the AbstractEntityCreator and CollisionSolver that will be owned and used by this InvaderEntityManager
	 */
    InvaderEntityManager(AbstractEntityCreator::sPtr AbstractEntityCreator, CollisionSolver::sPtr collisionSolver);

	void addSelfPtr(wPtr self);

	/**
	 * @brief	Creates and adds a single entity, determined by its name and position
	 */
	void add(const std::string& name, const Framework::Position& position);

	/**
	 * @brief	Adds an existing entity to the InvaderEntityManager
	 */
	void add(Framework::Entity::sPtr entity);

	/**
	 * @brief	Removes a certain entity from the Manager
	 */
	void remove(Framework::Entity::sPtr entity);

	/**
	 * @brief	Removes all entities that have been marked as "dead"
	 *
	 * Meant to be used once each "cycle", after updating all entities and checking for collisions.
	 * (see "_deleteContent")
	 */
	void removeAllDead();

	/**
	 * @brief	Checks if any entities collide with each other and solves the collision if needed
	 *
	 * When 2 colliding entities are found they are passed to the InvaderEntityManager's CollisionSolver, which
	 * determines the outcome.
	 */
	void checkCollision();

	/**
	 * @brief	Reports when an entity has been created elsewhere
	 */
	void notifyCreation(Framework::Entity::wPtr entity);

	/**
	 * @brief	Notify that an entity should be marked as "dead"
	 *
	 * This method puts the given entity in the removal queue, where it will be removed the next "cycle", by
	 * removeAllDead().
	 */
	void notifyDeath(Framework::Entity::wPtr entity);

	/**
	 * @brief	Creates an entity and adds it in response to a request from another class
	 */
	void notifyCreationRequest(const std::string& type, const Framework::Position& position);

private:
	/**
	 * Equivalent to the standard "this" pointer, but using a weak_ptr. Should be set when creating the
	 * InvaderEntityManager with its "make" method.
	 * This pointer is meant to be used when the InvaderEntityManager adds itself as a *Listener of another object.
	 */
	wPtr _this;

	/**
	 * All "living" entities are stored here.
	 */
	std::unordered_set<Framework::Entity::sPtr> _content;

	/**
	 * All entities flagged ad "dead" are put in here. They are all deleted at the end of each "cycle", after updating
	 * and checking for collisions.
	 */
	std::unordered_set<Framework::Entity::sPtr> _deleteContent;

	/**
	 * The factory used to create entities when requested.
	 */
    AbstractEntityCreator::sPtr _entityFactory;

	/**
	 * Used to determine what should happen when 2 entities collide.
	 */
	CollisionSolver::sPtr _collisionSolver;
};


/**
 * @class	EntityManagerException
 * @brief	Used to report all exceptions that happen within this InvaderEntityManager
 */
class EntityManagerException : public Framework::Exception {
public:
	EntityManagerException(std::string = "") noexcept;

	virtual ~EntityManagerException() noexcept;
};

} /* namespace game */

#endif /* ENTITYMANAGER_H_ */

/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include "InvaderGame.h"

namespace Game {

InvaderGame::sPtr InvaderGame::make(unsigned int resX, unsigned int resY, const std::string& properties) {
	// Work with raw pointer temporarily because shared_ptr can only access public methods.
	InvaderGame* game = new InvaderGame(resX, resY, properties);
	auto gameSPtr = std::shared_ptr<InvaderGame>(game);
	game->setSelfPtr(gameSPtr);
	return gameSPtr;
}

void InvaderGame::setSelfPtr(wPtr self) {
	_this = self;
}

InvaderGame::InvaderGame(unsigned int resX, unsigned int resY, const std::string& properties)  :
		_state(LOADING), _resX(resX), _resY(resY), _propertyLocation(properties), _showBackground(true),_showMiniMap(false)  {
}

void InvaderGame::run() {
	loop();
}

void InvaderGame::setup() {
	//Setup window ...
	_window.setFramerateLimit(60);
	_gameView = _window.getDefaultView();
	_defaultView = _window.getDefaultView();
	srand (time(NULL)); // Give seed for random generated numbers

	double internalResolution;
	if(_resY <= 480) {
		internalResolution = _resX*2;
	}
	else if(_resY <= 600) {
		internalResolution = _resX*1.75;
	}
	else {
		internalResolution = _resX*1.5;
	}

	Framework::Position::setResolution(internalResolution); // Set internal resolution

    _background = std::shared_ptr<Visual::Sfml::Background>(
            new Visual::Sfml::Background("./resources/background.png", _window, internalResolution));
    _titleBackground = std::shared_ptr<Visual::Sfml::Background>(
            new Visual::Sfml::Background("./resources/earthBig.png", _window, internalResolution));

    _viewManipulator = Visual::Sfml::GameViewManipulator::make(_gameView, _defaultView, _window.getSize().x, _showMiniMap);

	//Read the _propertyLocation of all entities ...
	Framework::PropertyFile entityPropertyDocument(_propertyLocation);
	auto entityProperties = entityPropertyDocument.parse();

	//Load all sprite textures (as specified in property document) ...
    _assetManager = std::shared_ptr<Visual::Sfml::AssetManager>(new Visual::Sfml::AssetManager());
	_assetManager->add(entityProperties,"sprite");
    _miniAssetManager = std::shared_ptr<Visual::Sfml::AssetManager>(new Visual::Sfml::AssetManager());
	_miniAssetManager->add(entityProperties,"mini");

	//Create entity and sprite factories for respective managers ...
    auto AbstractEntityCreator = std::shared_ptr<Game::AbstractEntityCreator>(new Game::InvaderEntityCreator(entityProperties));
    auto spriteCreator = std::shared_ptr<Visual::Sfml::SfmlSpriteCreator>(
            new Visual::Sfml::SfmlSpriteCreator(_assetManager, _window));
    auto miniSpriteCreator = std::shared_ptr<Visual::Sfml::SfmlSpriteCreator>( //creates sprites for minimap
            new Visual::Sfml::SfmlSpriteCreator(_miniAssetManager, _window,
					_window.getSize().x - Framework::Position::getResolution()/8, //offset from left side of the screen
					10, //offset from the top of the screen
					9)); //scale down to 1/9 of the regular screen size

	//Create collision solver ...
	auto collisionSolver = std::shared_ptr<Game::InvaderCollisionSolver>(new Game::InvaderCollisionSolver());

	//Create entity and sprite managers ...
    _entityManager = Game::InvaderEntityManager::make(AbstractEntityCreator, collisionSolver);
	_spriteManager = Visual::InvaderSpriteManager::make(spriteCreator);
	_miniSpriteManager = Visual::InvaderSpriteManager::make(miniSpriteCreator);

	//Create player controller ...
	_player1 = std::shared_ptr<Game::InvaderPlayerController>(new Game::InvaderPlayerController());

	//Set up links to entity manager ...
	_entityManager->addCreationListener(_spriteManager);
	_entityManager->addCreationListener(_miniSpriteManager);
	_entityManager->addCreationListener(_viewManipulator);
	_entityManager->addCreationListener(_player1);
	_entityManager->addDeathListener(_spriteManager);
	_entityManager->addDeathListener(_miniSpriteManager);
	_entityManager->addDeathListener(_viewManipulator);
	_entityManager->addDeathListener(_player1);

	//Read level selection information ...
	Framework::PropertyFile levelSelectPropertyDocument("./resources/menuProperties.ini");

	//Set up level select screen ...
    _levelSelection = Menu::InvaderMenu::make(_resX,_resY,"./resources/levels/");
	_levelSelection->addButtonSignalListener(_this);
    _levelSelectionVisualiser = std::shared_ptr<Menu::Sfml::InvaderMenuVisualiser>(
            new Menu::Sfml::InvaderMenuVisualiser(_window, "Pick a level:"));
	_levelSelection->addButtonCreationListener(_levelSelectionVisualiser);
	_levelSelection->buildMenu(levelSelectPropertyDocument.parse());

	//Set up HUD ...
    _lifeHud = std::shared_ptr<Hud::Sfml::HudLifeElement>(new Hud::Sfml::HudLifeElement(_window));
    _messageOverlay = std::shared_ptr<Hud::Sfml::ScreenOverlay>(new Hud::Sfml::ScreenOverlay(_window));
}

void InvaderGame::levelSelect(const std::string& levelLocation) {
	//Set level ...
	_currentLevel = std::shared_ptr<Game::LevelScenario>(new Game::LevelScenario());

	//Link level and entity manager ...
	_currentLevel->addCreationRequestListener(_entityManager);
	_entityManager->addDeathListener(_currentLevel);
	_currentLevel->addStatChangeListener(_lifeHud);
	_currentLevel->addMessageListener(_this);

	//Load level ...
	Framework::PropertyFile levelFile(levelLocation);
	auto levelData = levelFile.parse();
	_currentLevel->parseLookup(levelData);

	_state = GAME;
	_timer.restart();
}

void InvaderGame::quitLevel() {
	_viewManipulator->clear();
	_messageOverlay->clear();
	_lifeHud->clear();
	_entityManager->removeDeathListener(_currentLevel);
	_entityManager->clear();
	_spriteManager->clear();
	_miniSpriteManager->clear();
	_player1->clear();
	_state = LEVELSELECT;
}

void InvaderGame::pauseGame(bool pause) {
	switch(pause) {
	case true:
		_state = PAUSED;
		_messageOverlay->setText("PAUSED");
		break;
	case false:
		_messageOverlay->clear();
		_state = GAME;
		break;
	}
}

void InvaderGame::displayError(const char* what, const std::string& message) {
	_state = ERROR;
	std::string str = std::string(what);
	if(message != "") {
		str += ": " + message;
	}
	for(unsigned int i = 0; i<=str.length()/42; ++i) { //make sure message fits on screen
		str.insert(i*42,"\n");
	}
	str += "\n\n [press [esc] to quit]";
	_basicOverlay->setText("ERROR :(",-1,str);
}

void InvaderGame::handleInput() {
	//For movement real-time instead of event-based input is noticeably more fluid and responsive in SFML
	if(_state == GAME) {
		if(sf::Keyboard::isKeyPressed(sf::Keyboard::Left) || sf::Keyboard::isKeyPressed(sf::Keyboard::A)
				|| sf::Keyboard::isKeyPressed(sf::Keyboard::Q)) {
			_player1->keyPressed("LEFT");
		}
		if(sf::Keyboard::isKeyPressed(sf::Keyboard::Right) || sf::Keyboard::isKeyPressed(sf::Keyboard::D)) {
			_player1->keyPressed("RIGHT");
		}
		if(sf::Keyboard::isKeyPressed(sf::Keyboard::Space)) {
			_player1->keyPressed("SPACE");
		}
		if(sf::Keyboard::isKeyPressed(sf::Keyboard::X) || sf::Mouse::isButtonPressed(sf::Mouse::Left)) {
			_player1->keyPressed("X");
		}
	}

	sf::Event event;
	while (_window.pollEvent(event)){
		//Quit the game if necessary, regardless of what state it is in
		if (event.type == sf::Event::Closed) {
			_state = QUIT;
		}

		switch(_state) {
		// ///TITLE SCREEN/// //
		case TITLE:
			if (event.type == sf::Event::MouseButtonPressed)
			{
				_messageOverlay->clear();
				_state = LEVELSELECT;
			}
			else if(event.type == sf::Event::KeyPressed) {
				if(event.key.code == sf::Keyboard::Escape) {
					_state = QUIT;
				}
				else {
					_messageOverlay->clear();
					_state = LEVELSELECT;
				}
			}
			break;

		// ///LEVEL SELECTION SCREEN/// ///
		case LEVELSELECT:
			_window.setView(_window.getDefaultView());
			if(event.type == sf::Event::MouseMoved) {
				_levelSelection->mouseOver(event.mouseMove.x, event.mouseMove.y);
			}
			if (event.type == sf::Event::MouseButtonPressed)
			{
				if (event.mouseButton.button == sf::Mouse::Left) {
					_levelSelection->mouseClick(event.mouseButton.x, event.mouseButton.y);
				}
			}
			else if(event.type == sf::Event::KeyPressed) {
				if(event.key.code == sf::Keyboard::Return) {
					_levelSelection->buttonPress("ENTER");
				}
				if(event.key.code == sf::Keyboard::Escape) {
					_state = QUIT;
				}
			}
			else if (event.type == sf::Event::TextEntered)
			{
				if(event.text.unicode == '\b') {
					_levelSelection->textTyped("BACKSPACE");
				}
				else if(event.text.unicode == 13 || event.text.unicode == 27) {
					//ignore this, the user pressed enter or escape, should not be part of text
				}
				else if (event.text.unicode < 128) {
					sf::String str = event.text.unicode;
					_levelSelection->textTyped(str.toAnsiString());
				}
			}
			break;

		// ///THE MAIN GAME/// //
		case GAME:
			_window.setView(_gameView); //Necessary for correct detection of mouse position
			if(event.type == sf::Event::LostFocus) {
				pauseGame(true);
			}
			else if(event.type == sf::Event::KeyPressed) {
				if(event.key.code == sf::Keyboard::P || event.key.code == sf::Keyboard::Pause) {
					pauseGame(true);
				}
				if(event.key.code == sf::Keyboard::B) {
					_showBackground = !_showBackground;
				}
				if(event.key.code == sf::Keyboard::Escape) {
					quitLevel();
					_state = LEVELSELECT;
				}
			}
			else if(event.type == sf::Event::MouseMoved) {
				sf::Vector2f internalCoords=_window.mapPixelToCoords(
						sf::Vector2i(event.mouseMove.x,event.mouseMove.y));
				_player1->mouseMoved(internalCoords.x, internalCoords.y);
			}
			break;

		// ///THE GAME HAS BEEN PAUSED DIRECTLY OR INDIRECTLY BY THE USER/// //
		case PAUSED:
			if(event.type == sf::Event::GainedFocus) {
				pauseGame(false);
			}
			else if(event.type == sf::Event::KeyPressed) {
				if(event.key.code == sf::Keyboard::P || event.key.code == sf::Keyboard::Pause) {
					pauseGame(false);
				}
				if(event.key.code == sf::Keyboard::Escape) {
					quitLevel();
				}
			}
			break;

		// ///SOMETHING WENT WRONG/// //
		case ERROR:
			if (event.type == sf::Event::KeyPressed)
			{
				if(event.key.code == sf::Keyboard::Escape) {
					_state = QUIT;
				}
			}
			break;

		default:
			//do nothing
			break;
		}
	}
}

void InvaderGame::update() {
	switch(_state) {

	// ///THE LOADING SCREEN, SETUP OF ALL NECESSARY COMPONENTS/// //
	case LOADING:
		_window.clear();
	    _basicOverlay = std::shared_ptr<Hud::Sfml::ScreenOverlay>(new Hud::Sfml::ScreenOverlay(_window));
		_basicOverlay->setText("LOADING...");
		_basicOverlay->draw(0);
		_window.display();
		setup();
		_basicOverlay->setText("PROJECT: INVADER",-1,
				"---=====================================---\n\n\n\n\n\n"
				"             [press any key]");
		_state = TITLE;
		break;

	// ///TITLE SCREEN/// //
	case TITLE:
		_window.setView(_gameView);
		_titleBackground->draw();
		_window.setView(_window.getDefaultView());
		_basicOverlay->draw(_timer.getElapsedTime().asSeconds());
		break;

	// ///LEVEL SELECTION SCREEN/// ///
	case LEVELSELECT:
		_levelSelectionVisualiser->drawAll();
		break;

	// ///THE MAIN GAME/// //
	case GAME:
		//Update the level and all actors
		_currentLevel->update(_timer.getElapsedTime().asSeconds());
		_entityManager->updateAll(_timer.getElapsedTime().asSeconds());

		//Draw normal sprites on their own view
		_window.setView(_gameView);
		if(_showBackground) {
			_background->draw();
		}
		_spriteManager->drawAll();

		//HUD, Minimap and overlay get a different view because they need to remain at the same position on the screen
		_window.setView(_window.getDefaultView());
		if(_showMiniMap) {
			_miniSpriteManager->drawAll();
		}
		_lifeHud->draw();
		_messageOverlay->draw(_timer.getElapsedTime().asSeconds());
		break;

	// ///THE GAME HAS BEEN PAUSED DIRECTLY OR INDIRECTLY BY THE USER/// //
	case PAUSED:
		//Draw normal sprites on their own view
		_window.setView(_gameView);
		if(_showBackground) {
			_background->draw();
		}
		_spriteManager->drawAll();

		//Set view to default so the overlay will always have the same position on the screen
		_window.setView(_window.getDefaultView());
		_messageOverlay->draw(_timer.getElapsedTime().asSeconds());
		break;

		// ///SOMETHING WENT WRONG/// //
	case ERROR:
		_basicOverlay->draw(0);
		break;

	default:
		throw std::runtime_error("The game is not supposed to be in the current state at this time.");
	}
}

void InvaderGame::loop() {
	_window.create(sf::VideoMode(_resX, _resY), "Project Invader");

	_state = LOADING;
	_timer.restart();
	while (_window.isOpen() && _state != QUIT)
	{
		try{

		_window.clear();

		//Handle all user input
		handleInput();
		if(_state == QUIT) { //If user has just given a signal to quit the game
			break;
		}

		//Update all entities, game logic, and set elements to draw on screen
		update();

		//Draw frame
		_timer.restart();
		_window.display();

		} // end try
		catch(Framework::Exception& e) {
			displayError(e.what(), e.getMessage());
		}
		catch(std::exception& e) {
			displayError(e.what());
		}
	}

	if(_window.isOpen()) {
		_window.close();
	}

}

void InvaderGame::notifyMessage(const std::string& message, double duration, const std::string& extra) {
	_messageOverlay->setText(message, duration, extra);
}

void InvaderGame::notifyButtonSignal(const std::string& type, const std::string& content) {
	if(type == "LEVELSELECT") {
		levelSelect(content);
	}
	else if(type == "QUIT") {
		_state = QUIT;
	}
}

InvaderGame::~InvaderGame() {
}

} /* namespace game */

/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include "InvaderEntityManager.h"
#include <iostream>

namespace Game {

InvaderEntityManager::sPtr InvaderEntityManager::make(AbstractEntityCreator::sPtr AbstractEntityCreator,
		CollisionSolver::sPtr collisionSolver) {
	// Work with raw pointer temporarily because shared_ptr can only access public methods.
    InvaderEntityManager* manager = new InvaderEntityManager(AbstractEntityCreator, collisionSolver);
	auto managerSPtr = std::shared_ptr<InvaderEntityManager>(manager);
	manager->addSelfPtr(managerSPtr);
	return managerSPtr;
}

InvaderEntityManager::InvaderEntityManager(AbstractEntityCreator::sPtr AbstractEntityCreator,
		CollisionSolver::sPtr collisionSolver) :
        _entityFactory(AbstractEntityCreator), _collisionSolver(collisionSolver){

}

void InvaderEntityManager::addSelfPtr(wPtr self) {
	_this = self;
}

void InvaderEntityManager::add(const std::string& name, const Framework::Position& position) {
	add(_entityFactory->createEntity(name,position));
}

void InvaderEntityManager::add(Framework::Entity::sPtr entity) {
	_content.insert(entity);
	dispatchCreationEvent(entity);
	entity->addCreationRequestListener(_this);
	entity->addFinalDeathListener(_this);
}

void InvaderEntityManager::clear() {
	_deleteContent.clear();
	_content.clear();
}

void InvaderEntityManager::remove(Framework::Entity::sPtr entity) {
	auto find = _content.find(entity);
	if(find != _content.end()) {
		dispatchDeathEvent(entity);
		_content.erase(find);
	}
	else {
		throw EntityManagerException("Could not find entity to delete.");
	}
}

void InvaderEntityManager::removeAllDead() {
	for(auto& removeEntity : _deleteContent) {
		remove(removeEntity);
		if(removeEntity.use_count() != 1) {
			throw EntityManagerException("Entity could not be deleted completely.");
		}
	}
	_deleteContent.clear();
}

void InvaderEntityManager::updateAll(double timeDelta) {
	checkCollision();
	for(auto& entity : _content) {
		entity->update(timeDelta);
	}
	removeAllDead();
}

void InvaderEntityManager::checkCollision() {
	for(auto it = _content.begin(); it != _content.end(); ++it) {
		for(auto it2 = it; it2 != _content.end(); ++it2) {
			if(it == it2) {
				continue;
			}
            if((*it)->isNoCollision() || (*it2)->isNoCollision()) {
				continue;
			}
			if((*it)->collidesWith(*it2)) {
				_collisionSolver->solveCollision(*it, *it2);
			}
		}
	}
}

void InvaderEntityManager::notifyCreation(Framework::Entity::wPtr entity) {
	throw EntityManagerException("Entities should only be created by the InvaderEntityManager!");
}

void InvaderEntityManager::notifyDeath(Framework::Entity::wPtr entity) {
	_deleteContent.insert(entity.lock());
}

void InvaderEntityManager::notifyCreationRequest(const std::string& type, const Framework::Position& position) {
	add(type,position);
}

InvaderEntityManager::~InvaderEntityManager() {
}

EntityManagerException::EntityManagerException(std::string m)  noexcept:
			Exception("EntityManagementException", m) {

}

EntityManagerException::~EntityManagerException() noexcept{
}

} /* namespace game */

#ifndef COLLISIONSOLVER_H_
#define COLLISIONSOLVER_H_

/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include "../framework/Entity.h"

namespace Game {

/**
 * @class	CollisionSolver
 * @brief	Class used to determine the outcome of the collision of objects
 *
 * Abstract class, should be derived.
 */
class CollisionSolver {
public:
	typedef std::shared_ptr<CollisionSolver> sPtr;
	typedef std::weak_ptr<CollisionSolver> wPtr;

public:
	CollisionSolver();

	/**
	 * @brief	Takes 2 colliding entities and determines what should happen
	 *
	 * The outcome of collisions is decided by comparing the type of the entities
	 */
	virtual void solveCollision(Framework::Entity::wPtr entity1, Framework::Entity::wPtr entity2) = 0;

	virtual ~CollisionSolver();
};

} /* namespace game */

#endif /* COLLISIONSOLVER_H_ */

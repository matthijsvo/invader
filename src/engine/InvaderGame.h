#ifndef INVADERGAME_H_
#define INVADERGAME_H_

/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include <SFML/Graphics.hpp>

#include <stdlib.h>
#include <iostream>
#include <memory>
#include <string>
#include <stdlib.h>
#include <time.h>
#include "../framework/Messages.h"
#include "../framework/PropertyFile.h"
#include "../entities/InvaderEntityCreator.h"
#include "../entities/InvaderCollisionSolver.h"
#include "../entities/controllers/InvaderPlayerController.h"
#include "../engine/InvaderEntityManager.h"
#include "../visual/InvaderSpriteManager.h"
#include "../engine/LevelScenario.h"
#include "../sfml/AssetManager.h"
#include "../sfml/SfmlSpriteCreator.h"
#include "../sfml/GameViewManipulator.h"
#include "../sfml/Background.h"
#include "../hud/HudLifeElement.h"
#include "../hud/ScreenOverlay.h"
#include "../menu/InvaderMenu.h"
#include "../menu/InvaderMenuVisualiser.h"

namespace Game {

/**
 * @class	InvaderGame
 * @brief	The main class for the game, sets up everything and runs it
 *
 * This class should be used to run the game. It sets up everything, contains the game loop which updates
 * the game logic and handles the input and switching of levels.
 */
class InvaderGame : public Framework::MessageListener,
                    public Menu::ButtonSignalListener {
public:
	typedef std::shared_ptr<InvaderGame> sPtr;
	typedef std::weak_ptr<InvaderGame> wPtr;

public:
	/**
	 * @brief	Used as the constructor of InvaderGame, returns a shared_ptr to a new InvaderGame object
	 *
	 * This constructor, following the Named Constructor Idiom, makes sure that the InvaderGame is created as
	 * usual, but also sets a pointer to itself (see "_this").
	 */
	static sPtr make(unsigned int resX = 800, unsigned int resY = 600,
			const std::string& properties = "./resources/entityProperties.ini");

	/**
	 * @brief	Start the game
	 */
	void run();

	virtual ~InvaderGame();

private:
	/**
	 * @brief	Ctor of InvaderGame
	 *
	 * Creates a window with the specified resolution and uses the string "properties" to search for the specifications
	 * of all entities that are to be used in the game.
	 */
	InvaderGame(unsigned int resX = 800, unsigned int resY = 600,
			const std::string& properties = "./resources/entityProperties.ini");

	void setSelfPtr(wPtr self);

	/**
	 * @brief	Sets up all members of the InvaderGame object
	 */
	void setup();

	/**
	 * @brief	Sets up a new level at the specified location
	 */
	void levelSelect(const std::string& levelLocation);

	/**
	 * @brief	Pauses/Unpauses the game
	 */
	void pauseGame(bool pause);

	/**
	 * @brief	Makes sure all things specific to the current level are decoupled or erased
	 *
	 * This method should always be used before starting a new level.
	 */
	void quitLevel();

	/**
	 * @brief	When an exception occurs within the game loop, show the error on screen
	 */
	void displayError(const char* what, const std::string& message = "");

	/**
	 * @brief	Handle all user input
	 */
	void handleInput();

	/**
	 * @brief	Update all game entities, game logic, and set elements to draw on screen
	 */
	void update();

	/**
	 * @brief	The actual game loop.
	 */
	void loop();

	/**
	 * @brief	When another class notifies the InvaderGame with a message
	 *
	 * Prints the message as a screen overlay.
	 */
	void notifyMessage(const std::string& message, double duration, const std::string& extra);

	/**
	 * @brief	When a certain button gives a signal that needs to be seen by the InvaderGame
	 *
	 * This includes selecting a level or quitting the game
	 */
	void notifyButtonSignal(const std::string& type, const std::string& content);

private:
	/**
	 * The different states the game can be in.
	 */
	enum State{LOADING, TITLE, LEVELSELECT, GAME, PAUSED, QUIT, ERROR};

private:
	/**
	 * Equivalent to the standard "this" pointer, but using a weak_ptr. Should be set when creating the
	 * InvaderGame with its "make" method.
	 * This pointer is meant to be used when the InvaderGame adds itself as a *Listener of another object.
	 */
	wPtr _this;

	/**
	 * The current state of the game
	 */
	State _state;

	/**
	 * Width and height of the window
	 */
	unsigned int _resX;
	unsigned int _resY;

	/**
	 * Where the entity property file is located
	 */
	std::string _propertyLocation;

	/**
	 * A timer that times each cycle of the gameloop.
	 * With this reading Entities can update independently from the framerate of the game.
	 */
	sf::Clock _timer;

	/**
	 * The window of the game
	 */
	sf::RenderWindow _window;

	/**
	 * The "standard view", used for displaying HUD elements because they should always have the same position on the
	 * screen.
	 */
	sf::View _defaultView;

	/**
	 * This is the view in which the gameplay happens.
	 * Because this view needs to rotate when responding to certain user actions and therefore change its coördinates,
	 * it is unsuitable for displaying static elements like the HUD and needs to be separate.
	 */
	sf::View _gameView;

	/**
	 * Manipulates the "_gameView" in response to user actions.
	 */
    std::shared_ptr<Visual::Sfml::GameViewManipulator> _viewManipulator;

	/**
	 * Managers for entities, their visual representations and assets.
	 */
    std::shared_ptr<Visual::Sfml::AssetManager> _assetManager;
    std::shared_ptr<Visual::Sfml::AssetManager> _miniAssetManager;
    std::shared_ptr<Game::InvaderEntityManager> _entityManager;
    std::shared_ptr<Visual::InvaderSpriteManager> _spriteManager;
    std::shared_ptr<Visual::InvaderSpriteManager> _miniSpriteManager;

	/**
	 * Handles user input that controls certain entities in-game.
	 */
    std::shared_ptr<Game::InvaderPlayerController> _player1;

    std::shared_ptr<Game::LevelScenario> _currentLevel;

	/**
	 * HUD and Overlay elements
	 */
    std::shared_ptr<Hud::Sfml::HudLifeElement> _lifeHud;
    std::shared_ptr<Hud::Sfml::ScreenOverlay> _messageOverlay;
    std::shared_ptr<Visual::Sfml::Background> _background;
	bool _showBackground;
	bool _showMiniMap;

	/**
	 * Overlay for loading, title and error screens
	 */
    std::shared_ptr<Hud::Sfml::ScreenOverlay> _basicOverlay;
    std::shared_ptr<Visual::Sfml::Background> _titleBackground; //Background in the title screen

	/**
	 * The menu for selecting the level to play
	 */
    std::shared_ptr<Menu::InvaderMenu> _levelSelection;
    std::shared_ptr<Menu::Sfml::InvaderMenuVisualiser> _levelSelectionVisualiser;
};

} /* namespace game */

#endif /* INVADERGAME_H_ */

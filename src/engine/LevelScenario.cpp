/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include "LevelScenario.h"
namespace Game {

LevelScenario::LevelScenario() :
		_done(false), _finishedWaves(0), _timer(0), _inboundMessageShowing(0), _noEnemies(true), _enemyAmount(0) {
}

void LevelScenario::parseLookup(const Framework::PropertyFile::Lookup& lookup) {
	const auto foundLives = lookup.equal_range("lives");
	if(foundLives.first != lookup.end()) {
		for(auto it = foundLives.first; it!=foundLives.second; ++it){
			std::string trackThis = Framework::PropertyFile::returnAsString("entity",it->second);
			int livesLeft = Framework::PropertyFile::returnAsInt("amount",it->second);
			_lifeTracker.insert( std::pair<std::string,int>(trackThis, livesLeft) );
			dispatchStatChange(trackThis,livesLeft);
		}
	}

	const auto foundSetup = lookup.equal_range("setup");
	if(foundSetup.first != lookup.end()) {
		for(auto it = foundSetup.first; it!=foundSetup.second; ++it){
			std::string type = Framework::PropertyFile::returnAsString("entity",it->second);
			int amount = Framework::PropertyFile::returnAsInt("amount",it->second);
			double distance = Framework::PropertyFile::returnAsDouble("distanceToCenter",it->second);
			spawnSetup(type, amount, distance);
		}
	}

	const auto foundWaves = lookup.equal_range("wave");
	if(foundWaves.first != lookup.end()) {
		for(auto it = foundWaves.first; it!=foundWaves.second; ++it){
			double time = Framework::PropertyFile::returnAsDouble("time",it->second);
			bool enemy = Framework::PropertyFile::returnAsBool("enemy",it->second);
			std::string entityType = Framework::PropertyFile::returnAsString("entityType",it->second);
			int amount= Framework::PropertyFile::returnAsInt("amount",it->second);
			int rotation = Framework::PropertyFile::returnAsInt("rotation",it->second);
			double spread = Framework::PropertyFile::returnAsDouble("spread",it->second);

			if(enemy == true) {
				if(_noEnemies) {
					_noEnemies = false;
				}
				_enemyAmount += amount;
				if(_enemyTracker.find(entityType) == _enemyTracker.end()) {
					_enemyTracker.insert(entityType);
				}
			}

			_waves.insert( Wave(time, enemy, entityType, amount, rotation, spread) );
		}
	}
	else {
		_done = true;
		_finishedWaves = true;
	}
}

void LevelScenario::update(double timeDelta) {
	if(!_done) {
		_timer += timeDelta;
		if(!_finishedWaves) {
			auto& nextWave = *_waves.begin();

			//Show "Enemies Inbound" warning message
			if(nextWave._time < _timer+2) { //show a bit earlier than actual spawning
				if(!_inboundMessageShowing) { //don't dispatch again if already showing
					if(nextWave._enemy) {
						dispatchMessage("", 2.0, "[WARNING: ENEMIES INBOUND]");
					}
					else {
						dispatchMessage("", 2.0, "==[INCOMING REINFORCEMENTS]==");
					}
					_inboundMessageShowing = true;
				}
			}
			else if(_inboundMessageShowing) { //when not within above timespan anymore, make sure that it's turned off
				_inboundMessageShowing = false;
			}

			//Spawn new aliens
			if(nextWave._time < _timer) {
				spawnWave(nextWave);
				_waves.erase(_waves.begin());
			}
			if(_waves.empty()) {
				_finishedWaves = true;
			}
		}
		else if (!_noEnemies) {
			if(_enemyAmount == 0) { // All enemies have been killed
				dispatchMessage("GLORIOUS VICTORY", 20.0, "press [Esc] to go back");
				_done = true;
			}
		}
	}
}

void LevelScenario::spawnSetup(const std::string& type, int amount, double distance) {
	double internalResolution = Framework::Position::getResolution();
	double center = internalResolution/2;
	Framework::Position startPosition(center,center+distance,90); // Middle upper top of the window
	for(int i = 0; i < amount ; ++i) {
		dispatchCreationRequest(type,startPosition);
		startPosition.rotateAround(center,center,360/amount);
	}
}

void LevelScenario::spawnWave(const Wave& wave) {
	double internalResolution = Framework::Position::getResolution();
	double center = internalResolution/2;
	Framework::Position startPosition(center,center/8,90); // Middle upper top of the window
	startPosition.rotateAround(center,center,wave._rotation); // Set starting position to requested one
	for(int i = 0; i < wave._amount ; ++i) {
		dispatchCreationRequest(wave._enemyType,startPosition);
		startPosition.rotateAround(center,center,wave._spread/wave._amount);
	}
}

void LevelScenario::notifyDeath(Framework::Entity::wPtr entity) {
	auto isLiveTracked = _lifeTracker.find(entity.lock()->getName());
	if(isLiveTracked != _lifeTracker.end()){ // If this entity is one of the tracked ones
		if(isLiveTracked->second > 0) {
			--isLiveTracked->second;
			dispatchStatChange(entity.lock()->getName(), isLiveTracked->second);
			dispatchMessage("", 0.5, "-1 life");
		}
		else if(isLiveTracked->second == -1) {
			dispatchMessage("", 0.5, "-1 life");
		}

		if(isLiveTracked->second == 0) {
			dispatchMessage("YOU LOSE", 20.0, "press [Esc] to go back");
			_done = true; // We don't want to spoil the rest of the level now, do we? ;)
		}
		else { // Respawn if it has lives left
			dispatchCreationRequest(entity.lock()->getName(), entity.lock()->getPosition());
		}
	}
	else {
		auto isEnemyTracked = _enemyTracker.find(entity.lock()->getName());
		if(isEnemyTracked != _enemyTracker.end()) { // If this entity is one of the tracked enemy types
			--_enemyAmount;
		}
	}
}

LevelScenario::~LevelScenario() {
}

} /* namespace game */

/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include "AbstractEntityCreator.h"

namespace Game {

AbstractEntityCreator::AbstractEntityCreator(const Framework::PropertyFile::Lookup& lookup) : _lookup(lookup){
}

AbstractEntityCreator::~AbstractEntityCreator() {
}

EntityCreatorException::EntityCreatorException(std::string m)  noexcept:
			Exception("EntityCreationException", m) {

}

EntityCreatorException::~EntityCreatorException() noexcept{
}

} /* namespace game */

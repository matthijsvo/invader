#ifndef ABSTRACTENTITYCREATOR_H_
#define ABSTRACTENTITYCREATOR_H_

/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include "../framework/Entity.h"
#include "../framework/PropertyFile.h"

namespace Game {

/**
 * @class	AbstractEntityCreator
 * @brief	Factory that creates entities
 *
 * Abstract class, should be derived.
 */
class AbstractEntityCreator {
public:
    typedef std::shared_ptr<AbstractEntityCreator> sPtr;
    typedef std::weak_ptr<AbstractEntityCreator> wPtr;
public:
	/**
	 * @brief	Creates an entity when given its name (given by the user) and coördinates, and returns it
	 */
    virtual Framework::Entity::sPtr createEntity(const std::string& name, const Framework::Position& position) = 0;

    virtual ~AbstractEntityCreator();

protected:
    AbstractEntityCreator(const Framework::PropertyFile::Lookup& lookup);

protected:
	/**
	 * A lookup table for the creator.
	 * The creator uses this table to look up the different stats and attributes the created entities should have.
	 */
	Framework::PropertyFile::Lookup _lookup;
};


/**
 * @class	EntityCreatorException
 * @brief	Used to report exceptions that arise during the creation of entities
 */
class EntityCreatorException : public Framework::Exception {
public:
	EntityCreatorException(std::string = "") noexcept;
	virtual ~EntityCreatorException() noexcept;
};

} /* namespace game */

#endif /* ABSTRACTENTITYCREATOR_H_ */

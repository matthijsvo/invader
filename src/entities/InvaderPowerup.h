#ifndef INVADERPOWERUP_H_
#define INVADERPOWERUP_H_

/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include "../framework/Entity.h"
#include "InvaderPlayerShip.h"

namespace Game {

/**
 * @class	InvaderPowerup
 * @brief	Base class for all powerups
 *
 * Should be derived.
 * This class is the only link (via friendship) to InvaderPlayerShip.
 */
class InvaderPowerup : public Framework::Entity {
public:
	typedef std::shared_ptr<InvaderPowerup> sPtr;
	typedef std::weak_ptr<InvaderPowerup> wPtr;
public:
	/**
	 * @brief	Execute the wanted effect on an InvaderPlayerShip
	 */
	virtual void powerupEffect(InvaderPlayerShip::wPtr ship) = 0;

	void update(double timeDelta);

	virtual ~InvaderPowerup();

protected:
	InvaderPowerup(const std::string& name, const Framework::Position& position, double size, double speed);

	bool isDead();

private:
	double _speed;
};

} /* namespace game */

#endif /* INVADERPOWERUP_H_ */

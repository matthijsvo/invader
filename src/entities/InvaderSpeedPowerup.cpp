/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include "InvaderSpeedPowerup.h"

namespace Game {

InvaderSpeedPowerup::sPtr InvaderSpeedPowerup::make(const std::string& name, const Framework::Position& position,
		double size, double speed, double addSpeed) {
	InvaderSpeedPowerup* entity = new InvaderSpeedPowerup(name, position, size, speed, addSpeed);
	auto self = std::shared_ptr<InvaderSpeedPowerup>(entity);
	entity->setSelfPtr(self);
	return self;
}

InvaderSpeedPowerup::InvaderSpeedPowerup(const std::string& name, const Framework::Position& position, double size,
		double speed, double addSpeed) : InvaderPowerup(name,position,size,speed), _addSpeed(addSpeed){
}


void InvaderSpeedPowerup::powerupEffect(InvaderPlayerShip::wPtr ship) {
	ship.lock()->addSpeed(_addSpeed);
}

InvaderSpeedPowerup::~InvaderSpeedPowerup() {
}

} /* namespace game */

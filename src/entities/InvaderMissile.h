#ifndef INVADERMISSILE_H_
#define INVADERMISSILE_H_

/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include "InvaderProjectile.h"

namespace Game {

/**
 * @class	InvaderMissile
 * @brief	Target-tracking accelerating missile
 */
class InvaderMissile: public InvaderProjectile {
public:
	typedef std::shared_ptr<InvaderMissile> sPtr;
	typedef std::weak_ptr<InvaderMissile> wPtr;
public:
	static sPtr make(const std::string& name, const std::string& type, const Framework::Position& position, double size,
			int damage, double speed);

	void update(double timeDelta);

	/**
	 * @brief	Adjusts the missile's aim to the target's new position.
	 *
	 * Should be called when the mouse pointer changes position.
	 */
	void changeTarget(double x, double y);

	virtual ~InvaderMissile();
protected:
	InvaderMissile(const std::string& name, const std::string& type, const Framework::Position& position, double size,
			int damage, double speed);

	bool isDead();

private:
	/**
	 * Coordinates of the target
	 */
	double _targetX;
	double _targetY;

	double _currentSpeed;

	bool _reachedTarget;
};

} /* namespace game */

#endif /* INVADERMISSILE_H_ */

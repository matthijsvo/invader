#ifndef INVADERKAMIKAZESHIP_H_
#define INVADERKAMIKAZESHIP_H_

/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include "InvaderShip.h"

namespace Game {

/**
 * @class	InvaderKamikazeShip
 * @brief	Accelerating alien ship that goes straight to the center
 */
class InvaderKamikazeShip : public InvaderShip{
public:
	typedef std::shared_ptr<InvaderKamikazeShip> sPtr;
	typedef std::weak_ptr<InvaderKamikazeShip> wPtr;
public:
	static sPtr make(const std::string& name, const Framework::Position& position, double size, int health,
			double speed, const std::string& projectileType, unsigned int guns);

	void update(double timeDelta);

	virtual ~InvaderKamikazeShip();
protected:
	InvaderKamikazeShip(const std::string& name, const Framework::Position& position, double size, int health,
			double speed, const std::string& projectileType, unsigned int guns);
private:
	double _currentSpeed;
};

} /* namespace game */

#endif /* INVADERKAMIKAZESHIP_H_ */

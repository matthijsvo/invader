#ifndef INVADEREARTH_H_
#define INVADEREARTH_H_

/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include "InvaderEntity.h"

namespace Game {

/**
 * @class	InvaderPlanet
 * @brief	Very strong entity, capable of movement when not in the middle of the screen.
 *
 * Can also be an enemy.
 */
class InvaderPlanet : public InvaderEntity {
public:
	typedef std::shared_ptr<InvaderPlanet> sPtr;
	typedef std::weak_ptr<InvaderPlanet> wPtr;
public:
	static sPtr make(const std::string& name, const Framework::Position& position, double size, int health,
			bool enemy, double speed = 0);

	const bool isEnemy();

	void update(double timeDelta);

	virtual ~InvaderPlanet();
protected:
	InvaderPlanet(const std::string& name, const Framework::Position& position, double size, int health,
			bool enemy, double speed = 0);
private:
	bool _enemy;
};

} /* namespace game */

#endif /* INVADEREARTH_H_ */

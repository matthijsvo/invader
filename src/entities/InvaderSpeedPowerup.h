#ifndef INVADERSPEEDPOWERUP_H_
#define INVADERSPEEDPOWERUP_H_

/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include "InvaderPowerup.h"

namespace Game {

/**
 * @class	InvaderSpeedPowerup
 * @brief	Powerup that increases the speed of the player's ship
 */
class InvaderSpeedPowerup: public Game::InvaderPowerup {
public:
	typedef std::shared_ptr<InvaderSpeedPowerup> sPtr;
	typedef std::weak_ptr<InvaderSpeedPowerup> wPtr;
public:
	static sPtr make(const std::string& name, const Framework::Position& position, double size, double speed,
			double addSpeed = 10);

	/**
	 * @brief	Affect the given ship's attributes according to this powerup's effect.
	 */
	void powerupEffect(InvaderPlayerShip::wPtr ship);

	virtual ~InvaderSpeedPowerup();
protected:
	InvaderSpeedPowerup(const std::string& name, const Framework::Position& position, double size, double speed,
			double addSpeed = 10);
private:
	double _addSpeed;
};

} /* namespace game */

#endif /* INVADERSPEEDPOWERUP_H_ */

/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include "InvaderPlanet.h"

namespace Game {

InvaderPlanet::sPtr InvaderPlanet::make(const std::string& name, const Framework::Position& position, double size,
		int health, bool enemy, double speed) {
	InvaderPlanet* entity = new InvaderPlanet(name, position, size, health, enemy, speed);
	auto self = std::shared_ptr<InvaderPlanet>(entity);
	entity->setSelfPtr(self);
	return self;
}

InvaderPlanet::InvaderPlanet(const std::string& name, const Framework::Position& position, double size,
		int health, bool enemy, double speed) :
		InvaderEntity(name, position, size, health, speed), _enemy(enemy) {
	_type = "PLANET";
}

const bool InvaderPlanet::isEnemy() {
	return _enemy;
}

void InvaderPlanet::update(double timeDelta) {
	double center = _position.getResolution() / 2;
	double distance = timeDelta * _speed / 2000;
	_position.moveTowards(Framework::Position(center,center),distance); // Go straight to center of screen
	dispatchPositionEvent(_position);

	if(isDead()) {
		kill();
	}
}

InvaderPlanet::~InvaderPlanet() {
}

} /* namespace game */

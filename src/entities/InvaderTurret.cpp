/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include "InvaderTurret.h"

namespace Game {

InvaderTurret::sPtr InvaderTurret::make(const std::string& name, const Framework::Position& position, double size,
		const std::string& projectileType, double cooldown, int ammo) {
	InvaderTurret* entity = new InvaderTurret(name, position, size, projectileType, cooldown, ammo);
	auto self = std::shared_ptr<InvaderTurret>(entity);
	entity->setSelfPtr(self);
	return self;
}

InvaderTurret::InvaderTurret(const std::string& name, const Framework::Position& position, double size,
		const std::string& projectileType, double cooldown, int ammo) :
		Entity(name,position,size), _projectileType(projectileType), _cooldown(cooldown), _currentCooldown(0),
		_ammo(ammo) {
	_type = "TURRET";
}

void InvaderTurret::shoot() {
	if(_currentCooldown <= 0 && (_ammo > 0 || _ammo == -1)){
		dispatchCreationRequest(_projectileType,_position);
		_currentCooldown = _cooldown;
		if(_ammo != -1) {
			--_ammo;
		}
	}
}

void InvaderTurret::update(double timeDelta) {
	if(_currentCooldown > 0) {
		_currentCooldown -= timeDelta;
	}
}

InvaderTurret::~InvaderTurret() {
}

} /* namespace game */

/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include "InvaderEntityCreator.h"

namespace Game {

InvaderEntityCreator::InvaderEntityCreator(const Framework::PropertyFile::Lookup& lookup) : AbstractEntityCreator(lookup) {
}

Framework::Entity::sPtr InvaderEntityCreator::createEntity(const std::string& name, const Framework::Position& position){
	if(!Framework::PropertyFile::isEntryInLookup(name, _lookup)) {
		throw EntityCreatorException("Requested entity \""+name+
				"\" could not be found in entity database. Check your configuration files.");
	}

	Framework::Entity::sPtr entity;
	const auto found = _lookup.equal_range(name);
	if(found.first == _lookup.end()) {
		throw EntityCreatorException("No known entity type specified.");
	}
	const auto& lookupElement = found.first->second; //Only take first one

	std::string type = Framework::PropertyFile::returnAsString("type", lookupElement);

	if(type == "PLAYERSHIP") {
		int size = Framework::PropertyFile::returnAsInt("size", lookupElement)/2;
		int health = Framework::PropertyFile::returnAsInt("health", lookupElement);
		double speed = Framework::PropertyFile::returnAsDouble("speed", lookupElement);
		std::string projectileType = Framework::PropertyFile::returnAsString("projectileType", lookupElement);
		int guns = Framework::PropertyFile::returnAsInt("guns", lookupElement);
		double cooldown = Framework::PropertyFile::returnAsDouble("cooldown", lookupElement);
		entity = InvaderPlayerShip::make(name,position,size,health,speed,projectileType,guns,cooldown);
		entity->setNoCollisionTime(5.0); // On spawn 5 seconds invincibility
	}

	else if(type == "PLAYERBULLET") {
		int size = Framework::PropertyFile::returnAsInt("size", lookupElement)/2;
		int damage = Framework::PropertyFile::returnAsInt("damage", lookupElement);
		double speed = Framework::PropertyFile::returnAsDouble("speed", lookupElement);
		entity = InvaderProjectile::make(name,"PLAYERBULLET",position,size,damage,speed);
	}

	else if(type == "PLAYERMISSILE") {
		int size = Framework::PropertyFile::returnAsInt("size", lookupElement)/2;
		int damage = Framework::PropertyFile::returnAsInt("damage", lookupElement);
		double speed = Framework::PropertyFile::returnAsDouble("speed", lookupElement);
		entity = InvaderMissile::make(name,"PLAYERMISSILE",position,size,damage,speed);
	}

	else if(type == "TURRET") {
		std::string projectileType = Framework::PropertyFile::returnAsString("projectileType", lookupElement);
		double cooldown = Framework::PropertyFile::returnAsDouble("cooldown", lookupElement);
		int ammo = Framework::PropertyFile::returnAsInt("ammo", lookupElement);
		entity = InvaderTurret::make(name,position,0,projectileType,cooldown,ammo);
	}

	else if(type == "SHIELD") {
		int size = Framework::PropertyFile::returnAsInt("size", lookupElement)/2;
		int health = Framework::PropertyFile::returnAsInt("health", lookupElement);
		double speed = Framework::PropertyFile::returnAsDouble("speed", lookupElement);
		entity = InvaderShield::make(name,position,size,health,speed);
	}

	else if(type == "NORMALENEMYSHIP") {
		int size = Framework::PropertyFile::returnAsInt("size", lookupElement)/2;
		int health = Framework::PropertyFile::returnAsInt("health", lookupElement);
		double speed = Framework::PropertyFile::returnAsDouble("speed", lookupElement);
		std::string projectileType = Framework::PropertyFile::returnAsString("projectileType", lookupElement);
		int guns = Framework::PropertyFile::returnAsInt("guns", lookupElement);
		double forwardDistance = Framework::PropertyFile::returnAsDouble("forwardDistance", lookupElement);
		double turnDegrees = Framework::PropertyFile::returnAsDouble("turnDegrees", lookupElement);
		entity = InvaderEnemyShip::make(name,position,size,health,speed,projectileType,guns,forwardDistance,turnDegrees);
	}

	else if(type == "KAMIKAZESHIP") {
		int size = Framework::PropertyFile::returnAsInt("size", lookupElement)/2;
		int health = Framework::PropertyFile::returnAsInt("health", lookupElement);
		double speed = Framework::PropertyFile::returnAsDouble("speed", lookupElement);
		std::string projectileType = Framework::PropertyFile::returnAsString("projectileType", lookupElement);
		int guns = Framework::PropertyFile::returnAsInt("guns", lookupElement);
		entity = InvaderKamikazeShip::make(name,position,size,health,speed,projectileType,guns);
	}

	else if(type == "SPIRALSHIP") {
		int size = Framework::PropertyFile::returnAsInt("size", lookupElement)/2;
		int health = Framework::PropertyFile::returnAsInt("health", lookupElement);
		double speed = Framework::PropertyFile::returnAsDouble("speed", lookupElement);
		int guns = Framework::PropertyFile::returnAsInt("guns", lookupElement);
		std::string projectileType = Framework::PropertyFile::returnAsString("projectileType", lookupElement);
		entity = InvaderSpiralShip::make(name,position,size,health,speed,projectileType,guns);
	}

	else if(type == "FATMANSHIP") {
		int size = Framework::PropertyFile::returnAsInt("size", lookupElement)/2;
		int health = Framework::PropertyFile::returnAsInt("health", lookupElement);
		double speed = Framework::PropertyFile::returnAsDouble("speed", lookupElement);
		int guns = Framework::PropertyFile::returnAsInt("guns", lookupElement);
		std::string projectileType = Framework::PropertyFile::returnAsString("projectileType", lookupElement);
		entity = InvaderFatmanShip::make(name,position,size,health,speed,projectileType,guns);
	}

	else if(type == "ENEMYBULLET") {
		int size = Framework::PropertyFile::returnAsInt("size", lookupElement)/2;
		int damage = Framework::PropertyFile::returnAsInt("damage", lookupElement);
		double speed = Framework::PropertyFile::returnAsDouble("speed", lookupElement);
		entity = InvaderProjectile::make(name,"ENEMYBULLET",position,size,damage,speed);
	}

	else if(type == "PLANET") {
		int size = Framework::PropertyFile::returnAsInt("size", lookupElement)/2;
		int health = Framework::PropertyFile::returnAsInt("health", lookupElement);
		double speed =Framework::PropertyFile::returnAsDouble("speed", lookupElement);
		bool enemy = Framework::PropertyFile::returnAsBool("enemy", lookupElement);
		entity = InvaderPlanet::make(name,position,size,health,enemy,speed);
	}

	else if(type == "COOLDOWNPOWERUP") {
		int size = Framework::PropertyFile::returnAsInt("size", lookupElement)/2;
		double speed = Framework::PropertyFile::returnAsDouble("speed", lookupElement);
		double decreaseCooldown= Framework::PropertyFile::returnAsDouble("decreaseCooldown", lookupElement);
		entity = InvaderCooldownPowerup::make(name,position,size,speed,decreaseCooldown);
	}

	else if(type == "SPEEDPOWERUP") {
		int size = Framework::PropertyFile::returnAsInt("size", lookupElement)/2;
		double speed = Framework::PropertyFile::returnAsDouble("speed", lookupElement);
		double addSpeed= Framework::PropertyFile::returnAsDouble("addSpeed", lookupElement);
		entity = InvaderSpeedPowerup::make(name,position,size,speed,addSpeed);
	}

	else if(type == "ADDGUNPOWERUP") {
		int size = Framework::PropertyFile::returnAsInt("size", lookupElement)/2;
		double speed = Framework::PropertyFile::returnAsDouble("speed", lookupElement);
		int addGuns= Framework::PropertyFile::returnAsInt("addGuns", lookupElement);
		entity = InvaderAddGunPowerup::make(name,position,size,speed,addGuns);
	}
	else {
		throw EntityCreatorException("Unknown entity type specified. Check your configuration files.");
	}
	return entity;
}

InvaderEntityCreator::~InvaderEntityCreator() {
}

} /* namespace game */

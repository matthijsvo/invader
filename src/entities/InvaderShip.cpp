/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include "InvaderShip.h"

namespace Game {

InvaderShip::InvaderShip(const std::string& name, const Framework::Position& position, double size, int health,
		double speed, const std::string& projectileType, unsigned int guns) :
		InvaderEntity(name,position,size,health,speed), _projectileType(projectileType), _guns(guns) {
}

void InvaderShip::shoot() {
	unsigned int guns = _guns;
	if(guns % 2 == 1) { //when uneven amount, place gun in the middle and subtract to make it even
		dispatchCreationRequest(_projectileType,_position);
		--guns;
	}
	if(guns != 0) {
		double spacing = (_radius*3/5) / (guns/2);
		Framework::Position leftSpread = _position;
		Framework::Position rightSpread = _position;
		leftSpread.moveInDirection(_position.getRotation()+90,4,1,false);
		rightSpread.moveInDirection(_position.getRotation()-90,4,1,false);

		for(unsigned int i = 1; i <= guns/2; ++i) { //Spread out to left and right
			leftSpread.moveInDirection(_position.getRotation()+90,spacing*i,1,false);
			rightSpread.moveInDirection(_position.getRotation()-90,spacing*i,1,false);
			leftSpread.moveInDirection(_position.getRotation(),-spacing,1,false); //Bullets that are further away are
			rightSpread.moveInDirection(_position.getRotation(),-spacing,1,false);// also further back.

			dispatchCreationRequest(_projectileType,leftSpread); //Shoot bullet left and right per step outward
			dispatchCreationRequest(_projectileType,rightSpread);
		}
	}
}

InvaderShip::~InvaderShip() {
}

} /* namespace game */

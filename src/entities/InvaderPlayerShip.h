#ifndef INVADERPLAYERSHIP_H_
#define INVADERPLAYERSHIP_H_

/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include "InvaderShip.h"
#include "../framework/EventListener.h"

namespace Game {

/**
 * @class	InvaderPlayerShip
 * @brief	The player's ship
 */
class InvaderPlayerShip : public InvaderShip {
public:
	typedef std::shared_ptr<InvaderPlayerShip> sPtr;
	typedef std::weak_ptr<InvaderPlayerShip> wPtr;
public:
	static sPtr make(const std::string& name, const Framework::Position& position, double size, int health,
			double speed, const std::string& projectileType, unsigned int guns, double cooldown);

	void update(double timeDelta);

	void shoot();

	void changeDirection(bool clockwise);

	void addSpeed(double sp);
	void addGuns(int guns);
	void decreaseCooldown(double cool);

	virtual ~InvaderPlayerShip();
protected:
	InvaderPlayerShip(const std::string& name, const Framework::Position& position, double size, int health,
			double speed, const std::string& projectileType, unsigned int guns, double cooldown);
private:
	double _currentSpeed; //Positive is counter-clockwise, negative is clockwise (like degrees)

	/**
	 * Amount of time in between shots
	 */
	double _cooldown;

	/**
	 * Amount of time left to wait before it's possible to fire again
	 */
	double _currentCooldown;
};

} /* namespace game */

#endif /* INVADERPLAYERSHIP_H_ */

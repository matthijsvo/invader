#ifndef INVADERSHIELD_H_
#define INVADERSHIELD_H_

/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include "InvaderEntity.h"

namespace Game {

/**
 * @class	InvaderShield
 * @brief	Protects against aliens and their projectiles
 */
class InvaderShield : public InvaderEntity {
public:
	typedef std::shared_ptr<InvaderShield> sPtr;
	typedef std::weak_ptr<InvaderShield> wPtr;
public:
	static sPtr make(const std::string& name, const Framework::Position& position, double size, int health,
			double speed = 0);

	void update(double timeDelta);

	virtual ~InvaderShield();
protected:
	InvaderShield(const std::string& name, const Framework::Position& position, double size, int health,
			double speed = 0);
};

} /* namespace game */

#endif /* INVADERSHIELD_H_ */

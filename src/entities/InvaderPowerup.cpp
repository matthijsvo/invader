/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include "InvaderPowerup.h"

namespace Game {

InvaderPowerup::InvaderPowerup(const std::string& name, const Framework::Position& position, double size,
		double speed) :
		Framework::Entity(name,position,size), _speed(speed) {
	_type = "POWERUP";
}

void InvaderPowerup::update(double timeDelta) {
	double center = _position.getResolution() / 2;
	double distance = timeDelta * _speed * (_position.getResolution()/42);
	_position.moveTowards(Framework::Position(center,center),distance); //Go straight to center of screen.
	dispatchPositionEvent(_position);
	if(isDead() || _position.getDistance(center, center) < _position.getResolution()/1000) {
		kill();
	}
}

bool InvaderPowerup::isDead() {
	return _position.outsideBounds();
}

InvaderPowerup::~InvaderPowerup() {
}

} /* namespace game */

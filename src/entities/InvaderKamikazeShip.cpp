/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include "InvaderKamikazeShip.h"

namespace Game {

InvaderKamikazeShip::sPtr InvaderKamikazeShip::make(const std::string& name, const Framework::Position& position,
		double size, int health, double speed, const std::string& projectileType,
		unsigned int guns) {
	InvaderKamikazeShip* entity = new InvaderKamikazeShip(name, position, size, health, speed, projectileType, guns);
	auto self = std::shared_ptr<InvaderKamikazeShip>(entity);
	entity->setSelfPtr(self);
	return self;
}

InvaderKamikazeShip::InvaderKamikazeShip(const std::string& name, const Framework::Position& position,
		double size, int health, double speed, const std::string& projectileType,
		unsigned int guns) :
			InvaderShip(name,position,size,health,speed,projectileType,guns), _currentSpeed(0) {
	_type = "NORMALENEMYSHIP";
}

void InvaderKamikazeShip::update(double timeDelta) {
	double center = _position.getResolution() / 2;
	double distance = timeDelta * _currentSpeed;
	if(_currentSpeed <= _speed) {
		_currentSpeed += (_speed/1000);
	}
	_position.moveTowards(Framework::Position(center,center),distance);
	dispatchPositionEvent(_position);
	if(rand() % 1000 == 0) {
		shoot();
	}
	if(isDead() || _position.getDistance(center, center) < _position.getResolution()/1000) {
		kill();
	}
}

InvaderKamikazeShip::~InvaderKamikazeShip() {
}

} /* namespace game */

/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include "InvaderPlayerShip.h"

namespace Game {

InvaderPlayerShip::sPtr InvaderPlayerShip::make(const std::string& name, const Framework::Position& position,
		double size, int health, double speed, const std::string& projectileType, unsigned int guns, double cooldown) {
	InvaderPlayerShip* entity = new InvaderPlayerShip(name, position, size, health, speed, projectileType, guns,
			cooldown);
	auto self = std::shared_ptr<InvaderPlayerShip>(entity);
	entity->setSelfPtr(self);
	return self;
}

InvaderPlayerShip::InvaderPlayerShip(const std::string& name, const Framework::Position& position, double size,
		int health, double speed, const std::string& projectileType, unsigned int guns,
		double cooldown) :
				InvaderShip(name,position,size,health,speed,projectileType,guns), _currentSpeed(0), _cooldown(cooldown),
				_currentCooldown(0){
	_type = "PLAYERSHIP";
}

void InvaderPlayerShip::update(double timeDelta) {
	double center = _position.getResolution() / 2;

	if(_currentCooldown > 0) {
		_currentCooldown -= timeDelta;
	}
	_currentSpeed -= (_currentSpeed/10);
	double theta = _currentSpeed * timeDelta * 2;
	_position.rotateAround(center,center,theta);
	dispatchPositionEvent(_position);

	updateNoCollisionTime(timeDelta);

	if(isDead()) {
		kill();
	}
}

void InvaderPlayerShip::shoot() {
	if(_currentCooldown <= 0) {
		InvaderShip::shoot();
		_currentCooldown = _cooldown;
	}
}

void InvaderPlayerShip::changeDirection(bool clockwise) {
	if(!clockwise) {
		_currentSpeed = _speed;
	}
	else {
		_currentSpeed = -_speed;
	}
}

void InvaderPlayerShip::addSpeed(double sp) {
	_speed += sp;
}

void InvaderPlayerShip::addGuns(int guns) {
	_guns += guns;
	if(_guns < 0) {
		_guns = 0;
	}
}

void InvaderPlayerShip::decreaseCooldown(double cool) {
	_cooldown -= cool;
	if(_cooldown < 0) {
		_cooldown = 0;
	}
}

InvaderPlayerShip::~InvaderPlayerShip() {
}

} /* namespace game */

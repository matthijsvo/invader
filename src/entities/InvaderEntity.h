#ifndef INVADERENTITY_H_
#define INVADERENTITY_H_

/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include "../framework/Entity.h"

namespace Game {

/**
 * @class	InvaderEntity
 * @brief	Base class for all entities that can move and have health
 */
class InvaderEntity: public Framework::Entity {
public:
	void doDamage(int dam);

	const int getHealth();

	virtual void update(double timeDelta);

	virtual ~InvaderEntity();

protected:
	InvaderEntity(const std::string& name, const Framework::Position& position, double size, int health,
			double speed);

	/**
	 * @brief	Return if the InvaderEntitie's health is down to 0
	 */
	virtual bool isDead();

protected:
	int _health;
	double _speed;

};

} /* namespace game */

#endif /* INVADERENTITY_H_ */

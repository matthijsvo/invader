/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include "InvaderEnemyShip.h"

namespace Game {

InvaderEnemyShip::sPtr InvaderEnemyShip::make(const std::string& name, const Framework::Position& position,
		double size, int health, double speed, const std::string& projectileType, unsigned int guns, double forwardDistance,
		double turnDegrees) {
	InvaderEnemyShip* entity = new InvaderEnemyShip(name, position, size, health, speed, projectileType, guns,
			forwardDistance, turnDegrees);
	auto self = std::shared_ptr<InvaderEnemyShip>(entity);
	entity->setSelfPtr(self);
	return self;
}

InvaderEnemyShip::InvaderEnemyShip(const std::string& name, const Framework::Position& position, double size,
		int health, double speed, const std::string& projectileType, unsigned int guns,
		double forwardDistance, double turnDegrees) :
				InvaderShip(name,position,size,health,speed,projectileType,guns), _turning(true), _clockwise(true),
				_maxForwardDistance(forwardDistance), _forwardDistance(0), _maxDegreesTurned(turnDegrees),
				_degreesTurned(0) {
	_type = "NORMALENEMYSHIP";
}

void InvaderEnemyShip::update(double timeDelta) {
	double center = _position.getResolution() / 2;

	if(_turning) {
		double theta = timeDelta * _speed;
		if(_clockwise) {
			_position.rotateAround(center, center, theta);
			_degreesTurned += theta;
			if(_degreesTurned > _maxDegreesTurned) {
				_clockwise = false;
				_turning = false;
			}
		}
		else {
			_position.rotateAround(center, center, -theta);
			_degreesTurned -= theta;
			if(_degreesTurned < 0.0) {
				_clockwise = true;
				_turning = false;
			}
		}
	}
	else { //If not busy turning, go forward
		double distance = timeDelta * _speed;
		_position.moveTowards(Framework::Position(center, center), distance);
		_forwardDistance += distance;
		if(_forwardDistance > _maxForwardDistance) {
			_forwardDistance = 0;
			_turning = true;
		}
	}

	dispatchPositionEvent(_position);

	if(rand() % 1000 == 0) {
		shoot();
	}

	if(isDead() || _position.getDistance(center, center) < _position.getResolution()/1000) {
		kill();
	}
}

InvaderEnemyShip::~InvaderEnemyShip() {
}

} /* namespace game */

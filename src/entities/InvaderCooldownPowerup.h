#ifndef INVADERCOOLDOWNPOWERUP_H_
#define INVADERCOOLDOWNPOWERUP_H_

/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include "InvaderPowerup.h"

namespace Game {

/**
 * @class	InvaderCooldownPowerup
 * @brief	Powerup that makes the player's guns cool down faster
 */
class InvaderCooldownPowerup: public Game::InvaderPowerup {
public:
	typedef std::shared_ptr<InvaderCooldownPowerup> sPtr;
	typedef std::weak_ptr<InvaderCooldownPowerup> wPtr;
public:
	static sPtr make(const std::string& name, const Framework::Position& position, double size,
			double speed, double decreaseCooldown);

	/**
	 * @brief	Affect the given ship's attributes according to this powerup's effect.
	 */
	void powerupEffect(InvaderPlayerShip::wPtr ship);

	virtual ~InvaderCooldownPowerup();
protected:
	InvaderCooldownPowerup(const std::string& name, const Framework::Position& position, double size,
			double speed, double decreaseCooldown);
private:
	double _decreaseCooldown;
};

} /* namespace game */

#endif /* INVADERCOOLDOWNPOWERUP_H_ */

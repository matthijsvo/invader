/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include "InvaderCollisionSolver.h"

namespace Game {

InvaderCollisionSolver::InvaderCollisionSolver() {
}

void InvaderCollisionSolver::solveCollision(Framework::Entity::wPtr entity1, Framework::Entity::wPtr entity2) {
	std::string type1 = entity1.lock()->getType();
	std::string type2 = entity2.lock()->getType();
	//WEAPONS
	if(type1 == "PLAYERBULLET") {
		if(type2 == "NORMALENEMYSHIP") {
			projectileToEntity(entity1, entity2);
		}
	}
	else if(type1 == "PLAYERMISSILE") {
		if(type2 == "NORMALENEMYSHIP") {
			projectileToEntity(entity1, entity2);
		}
		else if(type2 == "ENEMYBULLET") {
			killBoth(entity1, entity2);
		}
	}
	else if(type1 == "ENEMYBULLET") {
		if(type2 == "PLAYERSHIP" || type2 == "SHIELD") {
			projectileToEntity(entity1, entity2);
		}
		else if(type2 == "PLAYERMISSILE") {
			killBoth(entity1, entity2);
		}
		else if(type2 == "PLANET") {
			entity1.lock()->kill();
		}
	}

	//SHIPS
	else if(type1 == "PLAYERSHIP") {
		if(type2 == "ENEMYBULLET") {
			projectileToEntity(entity2, entity1);
		}
		else if(type2 == "NORMALENEMYSHIP") {
			killBoth(entity1, entity2);
		}
		else if(type2 == "PLANET") {
			InvaderPlanet::wPtr planet = std::dynamic_pointer_cast<InvaderPlanet>(entity2.lock());
			if(planet.lock()->isEnemy()) {
				entityToStrongEntity(entity1, entity2);
			}
		}
		else if(type2 == "POWERUP") {
			InvaderPowerup::wPtr powerup = std::dynamic_pointer_cast<InvaderPowerup>(entity2.lock());
			InvaderPlayerShip::wPtr ship = std::dynamic_pointer_cast<InvaderPlayerShip>(entity1.lock());
			powerup.lock()->powerupEffect(ship);
			entity2.lock()->kill();
		}
	}
	else if(type1 == "NORMALENEMYSHIP") {
		if(type2 == "PLAYERBULLET" || type2 == "PLAYERMISSILE" || type2 == "PLAYERSHIP") {
			projectileToEntity(entity2, entity1);
		}
		if(type2 == "SHIELD" || type2 == "PLANET") {
			entityToStrongEntity(entity1, entity2);
		}
	}

	//OTHER
	else if(type1 == "PLANET") {
		if(type2 == "PLANET") {
			killBoth(entity2, entity1);
		}
		else {
			solveCollision(entity2, entity1);
		}
	}
	else if(type1 == "SHIELD") {
		if(type2 == "NORMALENEMYSHIP" || type2 == "ENEMYBULLET") {
			entityToStrongEntity(entity2, entity1);
		}
	}
	else if(type1 == "POWERUP") {
		if(type2 == "PLAYERSHIP") {
			InvaderPowerup::wPtr powerup = std::dynamic_pointer_cast<InvaderPowerup>(entity1.lock());
			InvaderPlayerShip::wPtr ship = std::dynamic_pointer_cast<InvaderPlayerShip>(entity2.lock());
			powerup.lock()->powerupEffect(ship);
			entity1.lock()->kill();
		}
		else if(type2 == "PLANET") {
			entity1.lock()->kill();
		}
	}
}

void InvaderCollisionSolver::projectileToEntity(Framework::Entity::wPtr entity1,
												Framework::Entity::wPtr entity2) {
	std::static_pointer_cast<InvaderEntity>(entity2.lock())->doDamage(
		std::static_pointer_cast<InvaderProjectile>(entity1.lock())->getDamage());
	std::static_pointer_cast<InvaderEntity>(entity1.lock())->kill();
}

void InvaderCollisionSolver::entityToStrongEntity(Framework::Entity::wPtr entity1,
													Framework::Entity::wPtr entity2) {
	std::static_pointer_cast<InvaderEntity>(entity2.lock())->doDamage(
		std::static_pointer_cast<InvaderEntity>(entity1.lock())->getHealth());
	std::static_pointer_cast<InvaderEntity>(entity1.lock())->kill();
}

void InvaderCollisionSolver::killBoth(Framework::Entity::wPtr entity1, Framework::Entity::wPtr entity2) {
	std::static_pointer_cast<InvaderEntity>(entity1.lock())->kill();
	std::static_pointer_cast<InvaderEntity>(entity2.lock())->kill();
}

InvaderCollisionSolver::~InvaderCollisionSolver() {
}

} /* namespace game */

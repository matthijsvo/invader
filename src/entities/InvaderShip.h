#ifndef INVADERNORMALENEMY_H_
#define INVADERNORMALENEMY_H_

/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include "InvaderEntity.h"

namespace Game {

/**
 * @class	InvaderShip
 * @brief	A ship
 *
 * A base class for all ships with guns. Should be derived.
 */
class InvaderShip: public InvaderEntity {
public:
	typedef std::shared_ptr<InvaderShip> sPtr;
	typedef std::weak_ptr<InvaderShip> wPtr;
public:
	virtual void shoot();
	virtual ~InvaderShip();
protected:
	InvaderShip(const std::string& name, const Framework::Position& position, double size, int health,
			double speed, const std::string& projectileType, unsigned int guns = 1);
protected:
	std::string _projectileType;
	int _guns;
};

} /* namespace game */

#endif /* INVADERNORMALENEMY_H_ */

#ifndef INVADERCOLLISIONSOLVER_H_
#define INVADERCOLLISIONSOLVER_H_

/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include "../engine/CollisionSolver.h"
#include "InvaderPlanet.h"
#include "InvaderPlayerShip.h"
#include "InvaderProjectile.h"
#include "InvaderPowerup.h"

namespace Game {

/**
 * @class	InvaderCollisionSolver
 * @brief	Class used to determine the outcome of the collision of objects
 */
class InvaderCollisionSolver : public CollisionSolver{
public:
	typedef std::shared_ptr<InvaderCollisionSolver> sPtr;
	typedef std::weak_ptr<InvaderCollisionSolver> wPtr;
public:
	InvaderCollisionSolver();

	/**
	 * @brief	Determines what the outcome should be of the collision betweem the two given objects
	 */
	void solveCollision(Framework::Entity::wPtr entity1, Framework::Entity::wPtr entity2);

	virtual ~InvaderCollisionSolver();

private:
	/**
	 * @brief	Solves the collision between a projectile and another entity
	 *
	 * The first entity is the projectile, it will be destroyed.
	 * The second entity is another entity, it's health will be decremented with the damage of the projectile.
	 */
	void projectileToEntity(Framework::Entity::wPtr entity1, Framework::Entity::wPtr entity2);

	/**
	 * @brief	Solves the collision between a "weak" and a "strong" entity
	 *
	 * A "weak" entity will always be destroyed in contact with a "strong" entity.
	 * "weak" entities include all NORMALENEMYSHIPs.
	 * "strong" entities, like a SHIELD or PLANET, take damage the amount of health the "weak" entity had left.
	 */
	void entityToStrongEntity(Framework::Entity::wPtr entity1, Framework::Entity::wPtr entity2);

	/**
	 * @brief	Solves collisions where both entities need to be destroyed
	 */
	void killBoth(Framework::Entity::wPtr entity1, Framework::Entity::wPtr entity2);

};

} /* namespace game */

#endif /* INVADERCOLLISIONSOLVER_H_ */

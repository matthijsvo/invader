#ifndef INVADERENTITYCREATOR_H_
#define INVADERENTITYCREATOR_H_

/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include <map>
#include "../engine/AbstractEntityCreator.h"
#include "InvaderPlanet.h"
#include "InvaderEnemyShip.h"
#include "InvaderKamikazeShip.h"
#include "InvaderSpiralShip.h"
#include "InvaderFatmanShip.h"
#include "InvaderPlayerShip.h"
#include "InvaderShield.h"
#include "InvaderProjectile.h"
#include "InvaderMissile.h"
#include "InvaderTurret.h"
#include "InvaderSpeedPowerup.h"
#include "InvaderCooldownPowerup.h"
#include "InvaderAddGunPowerup.h"
//#include "controllers/InvaderPlayerController.h"

namespace Game {

/**
 * @class	InvaderEntityCreator
 * @brief	Factory that creates entities
 *
 * Concrete factory for creating specific entities.
 */
class InvaderEntityCreator: public AbstractEntityCreator {
public:
	typedef std::shared_ptr<InvaderEntityCreator> sPtr;
	typedef std::weak_ptr<InvaderEntityCreator> wPtr;
public:
	InvaderEntityCreator(const Framework::PropertyFile::Lookup& lookup);

	/**
	 * @brief	Creates an entity when given its name (given by the user) and coördinates, and returns it
	 */
	Framework::Entity::sPtr createEntity(const std::string& name, const Framework::Position& position);

	virtual ~InvaderEntityCreator();
};

} /* namespace game */

#endif /* INVADERENTITYCREATOR_H_ */

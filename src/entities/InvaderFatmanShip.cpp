/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include "InvaderFatmanShip.h"

namespace Game {

InvaderFatmanShip::sPtr InvaderFatmanShip::make(const std::string& name, const Framework::Position& position,
		double size, int health, double speed, const std::string& projectileType,
		unsigned int guns) {
	InvaderFatmanShip* entity = new InvaderFatmanShip(name, position, size, health, speed, projectileType, guns);
	auto self = std::shared_ptr<InvaderFatmanShip>(entity);
	entity->setSelfPtr(self);
	return self;
}

InvaderFatmanShip::InvaderFatmanShip(const std::string& name, const Framework::Position& position,
		double size, int health, double speed, const std::string& projectileType,
		unsigned int guns) :
			InvaderShip(name,position,size,health,speed,projectileType, guns) {
	_type = "NORMALENEMYSHIP";
}

void InvaderFatmanShip::update(double timeDelta) {
	double center = _position.getResolution() / 2;
	double distance = timeDelta * _speed;
	_position.moveTowards(Framework::Position(center,center),distance); // Go straight to center of screen
	dispatchPositionEvent(_position);

	if(rand() % 1000 == 0) {
		shoot();
	}
	if(isDead() || _position.getDistance(center, center) < _position.getResolution()/1000) {
		kill();
	}
}

InvaderFatmanShip::~InvaderFatmanShip() {
}

} /* namespace game */

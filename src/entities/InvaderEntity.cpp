/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include "InvaderEntity.h"

namespace Game {

InvaderEntity::InvaderEntity(const std::string& name, const Framework::Position& position, double size,
		int health, double speed) :
				Entity(name,position,size), _health(health), _speed(speed) {
}

const int InvaderEntity::getHealth() {
	return _health;
}

bool InvaderEntity::isDead() {
	return (_health < 0);
}

void InvaderEntity::doDamage(int dam) {
	_health -= dam;
}

void InvaderEntity::update(double timeDelta) {

}
InvaderEntity::~InvaderEntity() {
}

} /* namespace game */

/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include "InvaderSpiralShip.h"

namespace Game {

InvaderSpiralShip::sPtr InvaderSpiralShip::make(const std::string& name, const Framework::Position& position,
		double size, int health, double speed, const std::string& projectileType, unsigned int guns) {
	InvaderSpiralShip* entity = new InvaderSpiralShip(name, position, size, health, speed, projectileType, guns);
	auto self = std::shared_ptr<InvaderSpiralShip>(entity);
	entity->setSelfPtr(self);
	return self;
}

InvaderSpiralShip::InvaderSpiralShip(const std::string& name, const Framework::Position& position,
		double size, int health, double speed, const std::string& projectileType, unsigned int guns) :
			InvaderShip(name,position,size,health,speed,projectileType, guns) {
	_type = "NORMALENEMYSHIP";
}

void InvaderSpiralShip::update(double timeDelta) {
	double center = _position.getResolution() / 2;
	double theta = timeDelta * _speed;
	_position.rotateAround(center, center, theta); //Rotate and
	_position.moveTowards(center, center, theta); // move a bit closer to the center
												// Together this effectively "spirals" down.
	dispatchPositionEvent(_position);
	if(rand() % 1000 == 0) {
		shoot();
	}
	if(isDead() || _position.getDistance(center, center) < _position.getResolution()/1000) {
		kill();
	}
}

InvaderSpiralShip::~InvaderSpiralShip() {
}

} /* namespace game */

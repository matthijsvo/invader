/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include "InvaderShield.h"
#include <iostream>
namespace Game {

InvaderShield::sPtr InvaderShield::make(const std::string& name, const Framework::Position& position, double size,
		int health, double speed) {
	InvaderShield* entity = new InvaderShield(name, position, size, health, speed);
	auto self = std::shared_ptr<InvaderShield>(entity);
	entity->setSelfPtr(self);
	return self;
}

InvaderShield::InvaderShield(const std::string& name, const Framework::Position& position, double size,
		int health, double speed) :
		InvaderEntity(name,position,size,health,speed) {
	_type = "SHIELD";
}

void InvaderShield::update(double timeDelta) {
	double center = _position.getResolution() / 2;
	_position.rotateAround(center,center,timeDelta*_speed); //Rotate around center
	if(_position.outsideBounds()) {
		_health = 0;//kill
	}
	dispatchPositionEvent(_position);

	if(isDead()) {
		kill();
	}
}

InvaderShield::~InvaderShield() {
}

} /* namespace game */

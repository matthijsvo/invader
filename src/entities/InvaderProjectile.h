#ifndef INVADERPROJECTILE_H_
#define INVADERPROJECTILE_H_

/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include "../framework/Entity.h"

namespace Game {

/**
 * @class	InvaderProjectile
 * @brief	A small flying projectile
 *
 * Should be derived.
 */
class InvaderProjectile : public Framework::Entity {
public:
	typedef std::shared_ptr<InvaderProjectile> sPtr;
	typedef std::weak_ptr<InvaderProjectile> wPtr;
public:
	static sPtr make(const std::string& name, const std::string& type, const Framework::Position& position,
			double size, int damage, double speed);

	const int getDamage();

	void update(double timeDelta);

	bool isDead();

	virtual ~InvaderProjectile();
protected:
	InvaderProjectile(const std::string& name, const std::string& type, const Framework::Position& position,
			double size, int damage, double speed);
protected:
	int _damage;

	double _speed;
};

} /* namespace game */

#endif /* INVADERPROJECTILE_H_ */

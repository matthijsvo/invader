#ifndef INVADERENEMYSHIP_H_
#define INVADERENEMYSHIP_H_

/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include "InvaderShip.h"
#include <stdlib.h>

namespace Game {

/**
 * @class	InvaderEnemyShip
 * @brief	The standard alien ship
 *
 * Descends to the center of the screen while zigzagging. Shoots bullets.
 */
class InvaderEnemyShip: public InvaderShip {
public:
	typedef std::shared_ptr<InvaderEnemyShip> sPtr;
	typedef std::weak_ptr<InvaderEnemyShip> wPtr;
public:
	static sPtr make(const std::string& name, const Framework::Position& position, double size, int health,
			double speed, const std::string& projectileType, unsigned int guns, double forwardDistance,
			double turnDegrees);

	virtual void update(double timeDelta);

	virtual ~InvaderEnemyShip();
protected:
	InvaderEnemyShip(const std::string& name, const Framework::Position& position, double size, int health,
			double speed, const std::string& projectileType, unsigned int guns, double forwardDistance,
			double turnDegrees);

private:
	/**
	 * If true, the InvaderEnemyShip is busy rotating around the center.
	 * If false, the InvaderEnemyShip is getting closer to the center.
	 */
	bool _turning;

	/**
	 * Whether the InvaderEnemyShip is rotating in clockwise direction.
	 */
	bool _clockwise;

	/**
	 * The maximum amount of distance the InvaderEnemyShip can go forward before starting to rotate again.
	 */
	double _maxForwardDistance;

	/**
	 * The amount the InvaderEnemyShip has currently gone forward.
	 */
	double _forwardDistance;

	/**
	 * The maximum amount of degrees the InvaderEnemyShip can turn clockwise around the center of the screen.
	 */
	double _maxDegreesTurned;

	/**
	 * The amount of degrees the InvaderEnemyShip has currently rotated clockwise.
	 * This decreases back to zero when rotating counterclockwise.
	 */
	double _degreesTurned;

	/**
	 * The type of bullet the InvaderEnemyShip shoots.
	 */
	std::string _bulletType;
};

} /* namespace game */

#endif /* INVADERENEMYSHIP_H_ */

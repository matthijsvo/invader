#ifndef INVADERADDGUNPOWERUP_H_
#define INVADERADDGUNPOWERUP_H_

/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include "InvaderPowerup.h"

namespace Game {

/**
 * @class	InvaderAddGunPowerup
 * @brief	Powerup that adds an extra gun to the player's ship
 */
class InvaderAddGunPowerup: public Game::InvaderPowerup {
public:
	typedef std::shared_ptr<InvaderAddGunPowerup> sPtr;
	typedef std::weak_ptr<InvaderAddGunPowerup> wPtr;
public:
	static sPtr make(const std::string& name, const Framework::Position& position, double size,
			double speed, int addGuns);

	/**
	 * @brief	Affect the given ship's attributes according to this powerup's effect.
	 */
	void powerupEffect(InvaderPlayerShip::wPtr ship);

	virtual ~InvaderAddGunPowerup();
protected:
	InvaderAddGunPowerup(const std::string& name, const Framework::Position& position, double size,
			double speed, int addGuns);
private:
	int _addGuns;
};

} /* namespace menu */

#endif /* INVADERADDGUNPOWERUP_H_ */

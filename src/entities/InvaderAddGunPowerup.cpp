/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include "InvaderAddGunPowerup.h"

namespace Game {

InvaderAddGunPowerup::sPtr InvaderAddGunPowerup::make(const std::string& name, const Framework::Position& position,
		double size, double speed, int addGuns) {
	InvaderAddGunPowerup* entity = new InvaderAddGunPowerup(name, position, size, speed, addGuns);
	auto self = std::shared_ptr<InvaderAddGunPowerup>(entity);
	entity->setSelfPtr(self);
	return self;
}

InvaderAddGunPowerup::InvaderAddGunPowerup(const std::string& name, const Framework::Position& position,
		double size, double speed, int addGuns) :
				InvaderPowerup(name,position,size,speed), _addGuns(addGuns){
}

void InvaderAddGunPowerup::powerupEffect(InvaderPlayerShip::wPtr ship) {
	ship.lock()->addGuns(_addGuns);
}

InvaderAddGunPowerup::~InvaderAddGunPowerup() {
}

} /* namespace game */

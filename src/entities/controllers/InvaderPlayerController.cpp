#include "InvaderPlayerController.h"

namespace Game {

InvaderPlayerController::InvaderPlayerController() {
}

void InvaderPlayerController::keyPressed(const std::string& key) {
	if(key == "LEFT" || key == "A") {
		for(const auto& ship : _ships) {
			ship.lock()->changeDirection(true);
		}
	}
	else if(key == "RIGHT" || key == "D") {
		for(const auto& ship : _ships) {
			ship.lock()->changeDirection(false);
		}
	}
	else if(key == "SPACE" || key == "Z") {
		for(const auto& ship : _ships) {
			ship.lock()->shoot();
		}
	}
	else if(key == "SHIFT" || key == "X") {
		for(const auto& turret : _turrets) {
			turret.lock()->shoot();
		}
	}
	else {
		return;
	}
}

void InvaderPlayerController::mouseMoved(double x, double y) {
	for(const auto& missile : _missiles) {
		missile.lock()->changeTarget(x,y);
	}
}
void InvaderPlayerController::addShip(InvaderPlayerShip::wPtr ship) {
	_ships.insert(ship);
}
void InvaderPlayerController::removeShip(InvaderPlayerShip::wPtr ship) {
	_ships.erase(ship);
}

void InvaderPlayerController::addTurret(InvaderTurret::wPtr turret) {
	_turrets.insert(turret);
}
void InvaderPlayerController::removeTurret(InvaderTurret::wPtr turret) {
	_turrets.erase(turret);
}

void InvaderPlayerController::addMissile(InvaderMissile::wPtr missile) {
	_missiles.insert(missile);
}
void InvaderPlayerController::removeMissile(InvaderMissile::wPtr missile) {
	_missiles.erase(missile);
}

void InvaderPlayerController::clear() {
	_ships.clear();
	_turrets.clear();
	_missiles.clear();
}

void InvaderPlayerController::notifyCreation(Framework::Entity::wPtr entity) {
	std::string type = entity.lock()->getType();
	if(type == "PLAYERSHIP") {
		addShip(std::dynamic_pointer_cast<InvaderPlayerShip>(entity.lock()));
	}
	else if(type == "TURRET") {
		addTurret(std::dynamic_pointer_cast<InvaderTurret>(entity.lock()));
	}
	else if(type == "PLAYERMISSILE") {
		addMissile(std::dynamic_pointer_cast<InvaderMissile>(entity.lock()));
	}
}

void InvaderPlayerController::notifyDeath(Framework::Entity::wPtr entity) {
	std::string type = entity.lock()->getType();
	if(type == "PLAYERSHIP") {
		removeShip(std::dynamic_pointer_cast<InvaderPlayerShip>(entity.lock()));
	}
	else if(type == "TURRET") {
		removeTurret(std::dynamic_pointer_cast<InvaderTurret>(entity.lock()));
	}
	else if(type == "PLAYERMISSILE") {
		removeMissile(std::dynamic_pointer_cast<InvaderMissile>(entity.lock()));
	}
}

InvaderPlayerController::~InvaderPlayerController() {
}

} /* namespace game */

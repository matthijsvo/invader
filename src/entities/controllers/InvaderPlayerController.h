#ifndef INVADERPLAYERCONTROLLER_H_
#define INVADERPLAYERCONTROLLER_H_

#include "../../framework/Entity.h"
#include "../../framework/EventListener.h"
#include "../InvaderPlayerShip.h"
#include "../InvaderTurret.h"
#include "../InvaderMissile.h"

namespace Game {

class InvaderPlayerController : public Framework::EntityController,
								public Framework::CreationEventListener,
								public Framework::DeathEventListener {
public:
	typedef std::shared_ptr<InvaderPlayerController> sPtr;
	typedef std::weak_ptr<InvaderPlayerController> wPtr;
public:
	InvaderPlayerController();
	void keyPressed(const std::string& key);
	void mouseMoved(double x, double y);
	void addShip(InvaderPlayerShip::wPtr ship);
	void removeShip(InvaderPlayerShip::wPtr ship);
	void addTurret(InvaderTurret::wPtr turret);
	void removeTurret(InvaderTurret::wPtr turret);
	void addMissile(InvaderMissile::wPtr missile);
	void removeMissile(InvaderMissile::wPtr missile);
	void clear();

	void notifyCreation(Framework::Entity::wPtr entity);
	void notifyDeath(Framework::Entity::wPtr entity);

	virtual ~InvaderPlayerController();
private:
	std::set<InvaderPlayerShip::wPtr,std::owner_less<InvaderPlayerShip::wPtr>> _ships;
	std::set<InvaderTurret::wPtr,std::owner_less<InvaderTurret::wPtr>> _turrets;
	std::set<InvaderMissile::wPtr,std::owner_less<InvaderMissile::wPtr>> _missiles;
};

} /* namespace game */

#endif /* INVADERPLAYERCONTROLLER_H_ */

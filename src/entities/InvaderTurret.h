#ifndef INVADERTURRET_H_
#define INVADERTURRET_H_

/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include "../framework/Entity.h"

namespace Game {

/**
 * @class	InvaderTurret
 * @brief	Stationary, invincible turret that shoots missiles until it runs out of ammo
 */
class InvaderTurret : public Framework::Entity{
public:
	typedef std::shared_ptr<InvaderTurret> sPtr;
	typedef std::weak_ptr<InvaderTurret> wPtr;
public:
	static sPtr make(const std::string& name, const Framework::Position& position, double size,
			const std::string& projectileType, double cooldown, int ammo = -1);

	void shoot();

	void update(double timeDelta);

	virtual ~InvaderTurret();
protected:
	InvaderTurret(const std::string& name, const Framework::Position& position, double size,
			const std::string& projectileType, double cooldown, int ammo = -1);
private:
	std::string _projectileType;

	/**
	 * Amount of time in between shots
	 */
	double _cooldown;

	/**
	 * Amount of time left to wait before it's possible to fire again
	 */
	double _currentCooldown;

	int _ammo;
};

} /* namespace game */

#endif /* INVADERTURRET_H_ */

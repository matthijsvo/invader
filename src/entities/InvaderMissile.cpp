/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include "InvaderMissile.h"

namespace Game {

InvaderMissile::sPtr InvaderMissile::make(const std::string& name, const std::string& type,
		const Framework::Position& position, double size, int damage, double speed) {
	InvaderMissile* entity = new InvaderMissile(name, type, position, size, damage, speed);
	auto self = std::shared_ptr<InvaderMissile>(entity);
	entity->setSelfPtr(self);
	return self;
}

InvaderMissile::InvaderMissile(const std::string& name, const std::string& type, const Framework::Position& position,
		double size, int damage, double speed) :
				InvaderProjectile(name,type,position,size,damage,speed),
				_targetX(0), _targetY(0),_currentSpeed(0),_reachedTarget(false) {
}

void InvaderMissile::update(double timeDelta) {
	double resolution = _position.getResolution();

	if(_currentSpeed < _speed) {
		_currentSpeed += timeDelta * _speed / 5; //Speed up to full speed
	}
	double distance = timeDelta * _currentSpeed / resolution * 84;
	if(!_reachedTarget) { //Track and follow target
		double rotToTarget = _position.getRotationTowards(_targetX,_targetY);
		_position.moveInDirection(rotToTarget,distance,_currentSpeed);

		//If the missile is "close enough" to the target, consider it reached
		if(_position.getDistance(_targetX,_targetY) < _position.getResolution()/1000*_currentSpeed) {
			_reachedTarget = true;
		}
	}
	else {
		_position.move(distance, _currentSpeed); //Just fly forward
	}
	dispatchPositionEvent(_position);

	if(isDead()) {
		kill();
	}
}

bool InvaderMissile::isDead() {
	return _position.outsideBounds();
}

void InvaderMissile::changeTarget(double x, double y) {
	_targetX = x;
	_targetY = y;
	_reachedTarget = false;
}

InvaderMissile::~InvaderMissile() {
}

} /* namespace game */

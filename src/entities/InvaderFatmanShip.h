#ifndef INVADERFATMANSHIP_H_
#define INVADERFATMANSHIP_H_

/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include "InvaderShip.h"

namespace Game {

/**
 * @class	InvaderFatmanShip
 * @brief	A very large and powerful, but slow alien ship
 */
class InvaderFatmanShip: public Game::InvaderShip {
public:
	typedef std::shared_ptr<InvaderFatmanShip> sPtr;
	typedef std::weak_ptr<InvaderFatmanShip> wPtr;
public:
	static sPtr make(const std::string& name, const Framework::Position& position, double size, int health,
			double speed, const std::string& projectileType, unsigned int guns);

	void update(double timeDelta);

	virtual ~InvaderFatmanShip();
protected:
	InvaderFatmanShip(const std::string& name, const Framework::Position& position, double size, int health,
			double speed, const std::string& projectileType, unsigned int guns);
};

} /* namespace game */

#endif /* INVADERFATMANSHIP_H_ */

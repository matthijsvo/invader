/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include "InvaderCooldownPowerup.h"

namespace Game {

InvaderCooldownPowerup::sPtr InvaderCooldownPowerup::make(const std::string& name, const Framework::Position& position,
		double size, double speed, double decreaseCooldown) {
	InvaderCooldownPowerup* entity = new InvaderCooldownPowerup(name, position, size, speed, decreaseCooldown);
	auto self = std::shared_ptr<InvaderCooldownPowerup>(entity);
	entity->setSelfPtr(self);
	return self;
}

InvaderCooldownPowerup::InvaderCooldownPowerup(const std::string& name, const Framework::Position& position,
		double size, double speed, double decreaseCooldown) :
				InvaderPowerup(name,position,size,speed), _decreaseCooldown(decreaseCooldown){
}

void InvaderCooldownPowerup::powerupEffect(InvaderPlayerShip::wPtr ship) {
	ship.lock()->decreaseCooldown(_decreaseCooldown);
}

InvaderCooldownPowerup::~InvaderCooldownPowerup() {
}

} /* namespace game */

/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include "InvaderProjectile.h"

namespace Game {

InvaderProjectile::sPtr InvaderProjectile::make(const std::string& name, const std::string& type,
		const Framework::Position& position, double size, int damage, double speed) {
	InvaderProjectile* entity = new InvaderProjectile(name, type, position, size, damage, speed);
	auto self = std::shared_ptr<InvaderProjectile>(entity);
	entity->setSelfPtr(self);
	return self;
}

InvaderProjectile::InvaderProjectile(const std::string& name, const std::string& type, const Framework::Position& position,
		double size, int damage, double speed) :
		Framework::Entity(name,position,size), _damage(damage), _speed(speed) {
	_type = type;
}

const int InvaderProjectile::getDamage() {
	return _damage;
}

void InvaderProjectile::update(double timeDelta) {
	double distance = timeDelta * _speed * (_position.getResolution()/42);
	_position.move(distance); // Move forward in its current direction
	dispatchPositionEvent(_position);

	if(isDead()) {
		kill();
	}
}

bool InvaderProjectile::isDead() {
	return _position.outsideBounds();
}

InvaderProjectile::~InvaderProjectile() {
}

} /* namespace game */

#ifndef INVADERSPIRALSHIP_H_
#define INVADERSPIRALSHIP_H_

/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include "InvaderShip.h"

namespace Game {

/**
 * @class	InvaderSpriralShip
 * @brief	Alien ship that spirals down to the center
 */
class InvaderSpiralShip : public InvaderShip{
public:
	typedef std::shared_ptr<InvaderSpiralShip> sPtr;
	typedef std::weak_ptr<InvaderSpiralShip> wPtr;
public:
	static sPtr make(const std::string& name, const Framework::Position& position, double size, int health,
			double speed, const std::string& projectileType, unsigned int guns);

	void update(double timeDelta);

	virtual ~InvaderSpiralShip();
protected:
	InvaderSpiralShip(const std::string& name, const Framework::Position& position, double size, int health,
			double speed, const std::string& projectileType, unsigned int guns);
};

} /* namespace game */

#endif /* INVADERSPIRALSHIP_H_ */

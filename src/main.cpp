/**
 * Main file, starts the game at a set resolution
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include "engine/InvaderGame.h"

int main(int argc, char** argv)
{
    Game::InvaderGame::sPtr game = Game::InvaderGame::make(800,600);
	game->run();
    return 0;
}

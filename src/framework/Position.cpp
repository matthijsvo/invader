/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include "Position.h"

namespace Framework {

double Position::_resolution = 1.0; //Initialize on 1;

Position::Position(double x, double y, double rot) :
		_x(x), _y(y), _rot(rot)  {
}

bool Position::operator==(const Position& other) {
	return (_x == other._x && _y == other._y);
}


const double Position::getX() const {
	return _x;
}

const double Position::getY() const {
	return _y;
}

const double Position::getRotation() const {
	return _rot;
}

const double Position::getDistance(const Position& pos) const {
	return sqrt(pow(pos.getX() - _x, 2) + pow(pos.getY() - _y, 2));
}

const double Position::getDistance(double x, double y) const {
	return sqrt(pow(x - _x, 2) + pow(y - _y, 2));
}

const double Position::getResolution() {
	return _resolution;
}

void Position::setResolution(double resolution) {
	_resolution = resolution;
}

void Position::setX(double x) {
	_x = x;
}

void Position::setY(double y) {
	_y = y;
}

void Position::setPosition(double x, double y) {
	setX(x);
	setY(y);
}

bool Position::outsideBounds() {
	return (_x < 0.0 || _x > _resolution || _y < 0.0 || _y > _resolution);
}

void Position::setRotation(double r) {
	_rot = correctRotation(r);
}

void Position::addRotation(double r) {
	_rot = correctRotation(_rot+r);
}

void Position::move(double distance, double factor) {
	moveInDirection(_rot, distance, factor);
}

void Position::moveInDirection(double rot,double distance,double factor, bool follow) {
	_x += factor*cos(rot*DEG_TO_RAD)*distance;
	_y += factor*sin(rot*DEG_TO_RAD)*distance;
	if(follow) {
		setRotation(rot);
	}
}

void Position::moveTowards(const Position& pos, double distance) {
	double theta = getRotationTowards(pos)*DEG_TO_RAD;
	_x += cos(theta)*distance;
	_y += sin(theta)*distance;
}

void Position::moveTowards(double x, double y, double distance) {
	double theta = getRotationTowards(x, y);
	moveInDirection(theta, distance);
}

const double Position::getRotationTowards(const Position& pos) {
	return getRotationTowards(pos.getX(),pos.getY());
}

const double Position::getRotationTowards(double x, double y) {
	return atan2(y - _y, x - _x)*RAD_TO_DEG;
}

const double Position::correctRotation(double rot) {
	return fmod(360+rot, 360);
}

void Position::rotateTowards(const Position& pos) {
	_rot = getRotationTowards(pos);
}

void Position::rotateTowards(double x, double y) {
	_rot = getRotationTowards(x,y);
}

void Position::rotateAround(double x, double y, double theta) {
	double radTheta = theta*DEG_TO_RAD;
	double nx = x + (_x-x)*cos(radTheta) - (_y - y)*sin(radTheta);
	double ny = y + (_x-x)*sin(radTheta) + (_y - y)*cos(radTheta);
	setPosition(nx,ny);

	addRotation(theta);
}

Position::~Position() {
}

} /* namespace framework */

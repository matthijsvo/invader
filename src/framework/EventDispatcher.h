#ifndef EVENTDISPATCHER_H_
#define EVENTDISPATCHER_H_

/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include "EventListener.h"
#include <set>

namespace Framework {

/**
 * *Dispatchers are part of the Observer Pattern; the Dispatcher being the "Observed".
 * The "Observer" is a *Listener (see EventListener.h).
 *
 * Notifies all listeners of certain changes.
 */

class CreationEventDispatcher {
public:
	typedef std::shared_ptr<CreationEventDispatcher> sPtr;
	typedef std::weak_ptr<CreationEventDispatcher> wPtr;
public:
	CreationEventDispatcher();
	virtual ~CreationEventDispatcher();
	void addCreationListener(std::weak_ptr<CreationEventListener> listener);
	void removeCreationListener(std::weak_ptr<CreationEventListener> listener);
protected:
	void dispatchCreationEvent(std::weak_ptr<Entity> entity);
private:
	std::set<std::weak_ptr<CreationEventListener>,std::owner_less<std::weak_ptr<CreationEventListener> > > _listeners;
};

class CreationRequestDispatcher {
public:
	typedef std::shared_ptr<CreationRequestDispatcher> sPtr;
	typedef std::weak_ptr<CreationRequestDispatcher> wPtr;
public:
	CreationRequestDispatcher();
	virtual ~CreationRequestDispatcher();
	void addCreationRequestListener(std::weak_ptr<CreationRequestListener> listener);
	void removeCreationRequestListener(std::weak_ptr<CreationRequestListener> listener);
protected:
	void dispatchCreationRequest(const std::string& type, const Position& position);
private:
	std::set<std::weak_ptr<CreationRequestListener>,std::owner_less<std::weak_ptr<CreationRequestListener> > >
		_listeners;
};

class DeathEventDispatcher {
public:
	typedef std::shared_ptr<DeathEventDispatcher> sPtr;
	typedef std::weak_ptr<DeathEventDispatcher> wPtr;
public:
	DeathEventDispatcher();
	virtual ~DeathEventDispatcher();
	void addDeathListener(std::weak_ptr<DeathEventListener> listener);
	void addFinalDeathListener(std::weak_ptr<DeathEventListener> listener);
	void removeDeathListener(std::weak_ptr<DeathEventListener> listener);
	void removeFinalDeathListener();
	void dispatchDeathEvent(std::weak_ptr<Entity> entity);
private:
	std::set<std::weak_ptr<DeathEventListener>,std::owner_less<std::weak_ptr<DeathEventListener> > > _listeners;
	std::weak_ptr<DeathEventListener> _finalListener; //Meant for owner of object, its shared_ptr
};

class PositionEventDispatcher {
public:
	typedef std::shared_ptr<PositionEventDispatcher> sPtr;
	typedef std::weak_ptr<PositionEventDispatcher> wPtr;
public:
	PositionEventDispatcher();
	virtual ~PositionEventDispatcher();
	void addPositionListener(std::weak_ptr<PositionEventListener> listener);
	void removePositionListener(std::weak_ptr<PositionEventListener> listener);
protected:
	void dispatchPositionEvent(const Position& position);
private:
	std::set<std::weak_ptr<PositionEventListener>,std::owner_less<std::weak_ptr<PositionEventListener> > > _listeners;
};

class StatChangeDispatcher {
public:
	typedef std::shared_ptr<PositionEventDispatcher> sPtr;
	typedef std::weak_ptr<PositionEventDispatcher> wPtr;
public:
	StatChangeDispatcher();
	virtual ~StatChangeDispatcher();
	void addStatChangeListener(std::weak_ptr<StatChangeListener> listener);
	void removeStatChangeListener(std::weak_ptr<StatChangeListener> listener);
protected:
	void dispatchStatChange(const std::string& statname, double stat);
private:
	std::set<std::weak_ptr<StatChangeListener>,std::owner_less<std::weak_ptr<StatChangeListener> > > _listeners;
};

class UserEventDispatcher {
public:
	typedef std::shared_ptr<UserEventDispatcher> sPtr;
	typedef std::weak_ptr<UserEventDispatcher> wPtr;
public:
	UserEventDispatcher();
	virtual ~UserEventDispatcher();
	void addUserListener(std::weak_ptr<UserEventListener> listener);
	void removeUserListener(std::weak_ptr<UserEventListener> listener);
protected:
	void dispatchUserKeyEvent(const std::string& key, double timeDelta);
	void dispatchUserMouseEvent(const std::string& button, double x, double y);
private:
	std::set<std::weak_ptr<UserEventListener>,std::owner_less<std::weak_ptr<UserEventListener> > > _listeners;
};

} /* namespace framework */

#endif /* EVENTDISPATCHER_H_ */

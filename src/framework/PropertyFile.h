#ifndef PROPERTYFILE_H_
#define PROPERTYFILE_H_

/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include <fstream>
#include <stdexcept>
#include <string>
#include <iostream>
#include <sstream>
#include <iomanip>
#include <algorithm>
#include <memory>
#include <map>
#include "Exception.h"

namespace Framework {

/**
 * @class	PropertyFile
 * @brief	Main class for all .ini file input
 */
class PropertyFile {
public:
	typedef std::shared_ptr<PropertyFile> sPtr;
	typedef std::weak_ptr<PropertyFile> wPtr;

	/**
	 * Stores all information from a file.
	 */
	typedef std::multimap<std::string,std::map<std::string,std::string> > Lookup;
public:
	PropertyFile(const std::string& filename);

	/**
	 * @brief	Load all information from the file into a Lookup object
	 */
	const Lookup parse();

	/**
	 * @brief	Searches one element of a Lookup for a certain entry and returns its value as an std::string
	 */
	static const std::string returnAsString(const std::string& find, const std::map<std::string,std::string>& table);

	/**
	 * @brief	Searches one element of a Lookup for a certain entry and returns its value as an integer
	 */
	static const int returnAsInt(const std::string& find, const std::map<std::string,std::string>& table);

	/**
	 * @brief	Searches one element of a Lookup for a certain entry and returns its value as a double
	 */
	static const double returnAsDouble(const std::string& find, const std::map<std::string,std::string>& table);

	/**
	 * @brief	Searches one element of a Lookup for a certain entry and returns its value as a boolean
	 */
	static const bool returnAsBool(const std::string& find, const std::map<std::string,std::string>& table);

	static const bool isEntryInLookup(const std::string& name, const Lookup& lookup);

	virtual ~PropertyFile();
private:
	/**
	 * @brief	Converts an std::string to a boolean
	 *
	 * Case of input is not important.
	 */
	static const bool toBool(std::string str);
private:
	/**
	 * The input file.
	 * This file is opened when a PropertyFile is created and closed when that PropertyFile is destroyed.
	 */
	std::ifstream _file;
};

/**
 * @class	ParseException
 * @brief	Exception class for reporting all errors during the parsing of a document and its elements
 */
class ParseException : public Exception {
public:
	ParseException(std::string = "") noexcept;

	virtual ~ParseException() noexcept;
};

} /* namespace framework */

#endif /* PROPERTYFILE_H_ */

#ifndef OBSERVER_H_
#define OBSERVER_H_

/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include <string>
#include <memory>

namespace Framework {

class Entity;
class Position;

/**
 * *Listeners are part of the Observer Pattern; the Listener being the "Observer".
 * The "Observed" is a *Dispatcher (see EventDispatcher.h).
 *
 * Responds to dispatched events by *Dispatchers.
 */


class CreationEventListener {
public:
	typedef std::shared_ptr<CreationEventListener> sPtr;
	typedef std::weak_ptr<CreationEventListener> wPtr;
public:
	virtual ~CreationEventListener() {}

	virtual void notifyCreation(std::weak_ptr<Entity> entity) = 0;
};

class CreationRequestListener {
public:
	typedef std::shared_ptr<CreationRequestListener> sPtr;
	typedef std::weak_ptr<CreationRequestListener> wPtr;
public:
	virtual ~CreationRequestListener() {}

	virtual void notifyCreationRequest(const std::string& type, const Position& position) = 0;
};

class DeathEventListener {
public:
	typedef std::shared_ptr<DeathEventListener> sPtr;
	typedef std::weak_ptr<DeathEventListener> wPtr;
public:
	virtual ~DeathEventListener() {}

	virtual void notifyDeath(std::weak_ptr<Entity> entity) = 0;
};

class PositionEventListener {
public:
	typedef std::shared_ptr<PositionEventListener> sPtr;
	typedef std::weak_ptr<PositionEventListener> wPtr;
public:
	virtual ~PositionEventListener() {}

	virtual void notifyPosition(const Position& position) = 0;
};

class StatChangeListener {
public:
	typedef std::shared_ptr<StatChangeListener> sPtr;
	typedef std::weak_ptr<StatChangeListener> wPtr;
public:
	virtual ~StatChangeListener() {}

	virtual void notifyStatChange(const std::string& statname, double stat) = 0;
};

class UserEventListener {
public:
	typedef std::shared_ptr<UserEventListener> sPtr;
	typedef std::weak_ptr<UserEventListener> wPtr;
public:
	virtual ~UserEventListener() {}

	virtual void notifyKey(const std::string& key, double timeDelta) = 0;
	virtual void notifyMouse(const std::string& button, double x, double y) = 0;
};

} /* namespace framework */

#endif /* OBSERVER_H_ */

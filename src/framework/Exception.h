#ifndef EXCEPTION_H_
#define EXCEPTION_H_

/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include <exception>
#include <string>

namespace Framework {

/**
 * @class Exception
 * @brief Base for all custom exceptions.
 */
class Exception : public std::exception {
public:
	Exception(std::string = "") noexcept;

	/**
	 * @brief	overload of \c what()
	 */
	virtual const char* what() const noexcept;

	/**
	 * @brief	Returns the more detailed message set by the constructor, if any.
	 */
	const std::string& getMessage() const noexcept;

	virtual ~Exception() noexcept;

protected:
	std::string _whatMessage;
	std::string _message;
	Exception(std::string, std::string) noexcept;
};

} /* namespace framework */

#endif /* EXCEPTION_H_ */

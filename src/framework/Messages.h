#ifndef MESSAGES_H_
#define MESSAGES_H_

/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include <memory>
#include <string>
#include <set>

namespace Framework {
/**
 * @class	MessageListener
 * @brief	Listens and responds to any sent messages
 *
 * Used to show messages and notifications on-screen
 */
class MessageListener {
public:
	typedef std::shared_ptr<MessageListener> sPtr;
	typedef std::weak_ptr<MessageListener> wPtr;
public:
	virtual ~MessageListener() {}

	virtual void notifyMessage(const std::string& message, double duration = -1, const std::string& misc = "") = 0;
};

/**
 * @class	MessageDispatcher
 * @brief	Used to send certain messages that can be shown on-screen
 */
class MessageDispatcher {
public:
	typedef std::shared_ptr<MessageDispatcher> sPtr;
	typedef std::weak_ptr<MessageDispatcher> wPtr;
public:
	MessageDispatcher();
	virtual ~MessageDispatcher();
	void addMessageListener(std::weak_ptr<MessageListener> listener);
	void removeMessageListener(std::weak_ptr<MessageListener> listener);
protected:
	/**
	 * @brief	Dispatches a main message that should be shown for a certain duration, and if necessary an extra string
	 *
	 * When the miscellaneous string is used and the message is left empty this is interpreted as a small notification
	 */
	void dispatchMessage(const std::string& message, double duration = -1, const std::string& misc = "");
private:
	std::set<std::weak_ptr<MessageListener>,std::owner_less<std::weak_ptr<MessageListener> > > _listeners;
};
}/* namespace framework */
#endif /* MESSAGES_H_ */

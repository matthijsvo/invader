/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include "Entity.h"

namespace Framework {

// ENTITY //

Entity::Entity(const std::string& name, const Framework::Position& position, double radius) :
		_name(name), _type("ENTITY"), _position(Position(position)), _radius(radius), _noCollisionTime(0) {
}

const std::string Entity::getName() const {
	return _name;
}

const std::string Entity::getType() const {
	return _type;
}

void Entity::kill() {
	dispatchDeathEvent(_this);
}

void Entity::setPosition(double x, double y) {
	_position = Position(x,y);
}

const Position& Entity::getPosition() {
	return _position;
}

void Entity::update(double timeDelta) {

}

void Entity::updateNoCollisionTime(double timeDelta) {
	if(_noCollisionTime > 0) {
		_noCollisionTime -= timeDelta;
	}
}

void Entity::setNoCollisionTime(double time) {
	_noCollisionTime = time;
}

bool Entity::isNoCollision() {
	return _noCollisionTime > 0;
}

bool Entity::collidesWith(wPtr entity) {
	return(_position.getDistance(entity.lock()->_position) - _radius - entity.lock()->_radius < 0);
}

Entity::~Entity() {
}

void Entity::setSelfPtr(sPtr self) {
	_this = self;
}

// ENTITYCONTROLLER //

EntityController::EntityController() {
}

EntityController::~EntityController() {
}

} /* namespace framework */


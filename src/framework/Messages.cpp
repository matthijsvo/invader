/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include "Messages.h"

namespace Framework {

MessageDispatcher::MessageDispatcher() {

}

void MessageDispatcher::addMessageListener(std::weak_ptr<MessageListener> listener) {
	_listeners.insert(listener);
}

void MessageDispatcher::removeMessageListener(std::weak_ptr<MessageListener> listener) {
	_listeners.erase(listener);
}

void MessageDispatcher::dispatchMessage(const std::string& message, double duration, const std::string& misc) {
	for(const auto& listener : _listeners) {
		listener.lock()->notifyMessage(message, duration, misc);
	}
}

MessageDispatcher::~MessageDispatcher() {

}

}/* namespace framework*/



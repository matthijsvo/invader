#ifndef ENTITY_H_
#define ENTITY_H_

/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include <memory>
#include <set>
#include <string>
#include <math.h>
#include <time.h>
#include <stdlib.h>
#include "Position.h"
#include "EventDispatcher.h"
#include "Exception.h"

namespace Framework {

class Entity;
class EntityController;

/**
 * @class	Entity
 * @brief	Base class for representing a single entity/model
 *
 * Base class for all entities in the game. Contains basic information like name, type and position.
 *
 * Only derived classes should be used.
 */
class Entity : public DeathEventDispatcher,
				public PositionEventDispatcher,
                public Framework::CreationRequestDispatcher {
public:
	typedef std::shared_ptr<Entity> sPtr;
	typedef std::weak_ptr<Entity> wPtr;

protected:
	/**
	 * @brief	Constructor for Entity
	 * @arg		name identifies the type of entity
	 */
    Entity(const std::string& name, const Framework::Position& position, double radius);

public:
	const std::string getName() const;

	const std::string getType() const;

	const Position& getPosition();

	/**
	 * @brief	Dispatches a signal that the entity is ready for removal
	 */
	void kill();

	void setPosition(double x, double y);

	/**
	 * @brief	This sets the time the entity will report no collisions
	 */
	void setNoCollisionTime(double time);

	/**
	 * @brief	Updates this Entity within a certain timeframe timeDelta
	 *
	 * This specific method does nothing, derived classes that need specific behaviour have to override this.
	 */
	virtual void update(double timeDelta);

	/**
	 * @brief	Decrease the time the entity reports no collisions
	 */
	void updateNoCollisionTime(double timeDelta);

    bool isNoCollision();

	/**
	 * @brief	Measures if this entity collides with another.
	 */
	virtual bool collidesWith(wPtr entity);

	/**
	 * @brief	Destructor of Entity
	 */
	virtual ~Entity();

protected:
	void setSelfPtr(sPtr self);

protected:
	/**
	 * Equivalent to the regular "this" pointer of a class, only with a weak_ptr.
	 * Has to be set by derived classes by using a contructor according to the Named Constructor Idiom that
	 * creates a weak_ptr and adds it with the setSelfPtr method.
	 * Setting this pointer is not necessary, but required for adding this entity as an Observer (in the Observer
	 * Design Pattern construction)
	 */
	wPtr _this;

	/**
	 * The name of an entity is determined by the user and used to communicate with the View.
	 */
	std::string _name;

	/**
	 * The type can be selected from a few basic types, they determine the behaviour of the entity and how it responds
	 * to collision with others.
	 */
	std::string _type;

	/**
	 * The position determines the place and rotation on an internal 2D plane. This plane does not necessarily have the
	 * same dimensions as the the window it is drawn on.
	 */
	Position _position;

	/**
	 * Radius of the entity, used for detecting collision.
	 */
	double _radius;

	/**
	 * Amount of time the entity will report no collisions.
	 */
	double _noCollisionTime;
};

/**
 * @class	EntityController
 * @brief	Base class EntityController is the controller for Entities
 * 			Part of the Model-View-Controller DP, used to issue commands to Entities
 *
 * Only derived classes should be used.
 */
class EntityController : public UserEventDispatcher{
public:
	/**
	 * @brief	Constructor for EntityController
	 */
	EntityController();

	/**
	 * @brief	Destructor for EntityController
	 */
	virtual ~EntityController();
};

} /* namespace framework */

#endif /* ENTITY_H_ */

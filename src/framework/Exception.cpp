/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include "Exception.h"

namespace Framework {

Exception::Exception(std::string m)  noexcept:
			Exception("Exception", m) {

}

Exception::Exception(std::string what, std::string m)  noexcept:
			_whatMessage(what), _message(m) {

}

const std::string& Exception::getMessage() const  noexcept{
	return _message;
}

const char* Exception::what() const  noexcept {
	return _whatMessage.c_str();
}

Exception::~Exception() noexcept{
}
} /* namespace framework */

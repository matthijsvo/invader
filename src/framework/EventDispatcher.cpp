/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include "EventDispatcher.h"

namespace Framework {

//	CREATION EVENT DISPATCHER //

CreationEventDispatcher::CreationEventDispatcher() {

}

void CreationEventDispatcher::addCreationListener(std::weak_ptr<CreationEventListener> listener) {
	_listeners.insert(listener);
}

void CreationEventDispatcher::removeCreationListener(std::weak_ptr<CreationEventListener> listener) {
	_listeners.erase(listener);
}

void CreationEventDispatcher::dispatchCreationEvent(std::weak_ptr<Entity> entity) {
	for(const auto& listener : _listeners) {
		listener.lock()->notifyCreation(entity);
	}
}

CreationEventDispatcher::~CreationEventDispatcher() {

}

//	CREATION REQUEST DISPATCHER //

CreationRequestDispatcher::CreationRequestDispatcher() {

}

void CreationRequestDispatcher::addCreationRequestListener(std::weak_ptr<CreationRequestListener> listener) {
	_listeners.insert(listener);
}

void CreationRequestDispatcher::removeCreationRequestListener(std::weak_ptr<CreationRequestListener> listener) {
	_listeners.erase(listener);
}

void CreationRequestDispatcher::dispatchCreationRequest(const std::string& type, const Position& position) {
	for(const auto& listener : _listeners) {
		listener.lock()->notifyCreationRequest(type,position);
	}
}

CreationRequestDispatcher::~CreationRequestDispatcher() {

}


//	DEATH EVENT DISPATCHER //

DeathEventDispatcher::DeathEventDispatcher() {

}

void DeathEventDispatcher::addDeathListener(std::weak_ptr<DeathEventListener> listener) {
	_listeners.insert(listener);
}

void DeathEventDispatcher::addFinalDeathListener(std::weak_ptr<DeathEventListener> listener) {
	_finalListener = listener;
}

void DeathEventDispatcher::removeDeathListener(std::weak_ptr<DeathEventListener> listener) {
	_listeners.erase(listener);
}

void DeathEventDispatcher::removeFinalDeathListener() {
	_finalListener.reset();
}

void DeathEventDispatcher::dispatchDeathEvent(std::weak_ptr<Entity> entity) {
	for(const auto& listener : _listeners) {
		listener.lock()->notifyDeath(entity);
	}
	if(!_finalListener.expired()) {
		_finalListener.lock()->notifyDeath(entity);
	}
}

DeathEventDispatcher::~DeathEventDispatcher() {

}


//	POSITION EVENT DISPATCHER //

PositionEventDispatcher::PositionEventDispatcher() {

}

void PositionEventDispatcher::addPositionListener(std::weak_ptr<PositionEventListener> listener) {
	_listeners.insert(listener);
}

void PositionEventDispatcher::removePositionListener(std::weak_ptr<PositionEventListener> listener) {
	_listeners.erase(listener);
}

void PositionEventDispatcher::dispatchPositionEvent(const Position& position) {
	for(const auto& listener : _listeners) {
		listener.lock()->notifyPosition(position);
	}
}

PositionEventDispatcher::~PositionEventDispatcher() {

}


//	STAT CHANGE DISPATCHER //

StatChangeDispatcher::StatChangeDispatcher() {

}

void StatChangeDispatcher::addStatChangeListener(std::weak_ptr<StatChangeListener> listener) {
	_listeners.insert(listener);
}

void StatChangeDispatcher::removeStatChangeListener(std::weak_ptr<StatChangeListener> listener) {
	_listeners.erase(listener);
}

void StatChangeDispatcher::dispatchStatChange(const std::string& statname, double stat) {
	for(const auto& listener : _listeners) {
		listener.lock()->notifyStatChange(statname, stat);
	}
}

StatChangeDispatcher::~StatChangeDispatcher() {

}

//	USER EVENT DISPATCHER //

UserEventDispatcher::UserEventDispatcher() {

}

void UserEventDispatcher::addUserListener(std::weak_ptr<UserEventListener> listener) {
	_listeners.insert(listener);
}

void UserEventDispatcher::removeUserListener(std::weak_ptr<UserEventListener> listener) {
	_listeners.erase(listener);
}

void UserEventDispatcher::dispatchUserKeyEvent(const std::string& key, double timeDelta) {
	for(const auto& listener : _listeners) {
		listener.lock()->notifyKey(key, timeDelta);
	}
}

void UserEventDispatcher::dispatchUserMouseEvent(const std::string& button, double x, double y) {
	for(const auto& listener : _listeners) {
		listener.lock()->notifyMouse(button, x, y);
	}
}

UserEventDispatcher::~UserEventDispatcher() {

}

} /* namespace framework */

/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include "PropertyFile.h"

namespace Framework {

PropertyFile::PropertyFile(const std::string& filename) {
	_file.open(filename);
	if (!_file.is_open()) {
		throw ParseException("Unable to open file.");
	}
}

const PropertyFile::Lookup PropertyFile::parse(){
	Lookup lookup;

	std::string buffer;
	std::string readLine;
	while(_file.good()){
		getline(_file, readLine);
		buffer.append(readLine);
		buffer.push_back('\n');
	}

	std::string nameTag;
	std::string elementName;
	std::string elementContent;
	std::map<std::string,std::string> elements;
	bool inNameTag = false;
	bool inElementContent = false;
	bool inComment = false;
	for(const char& ch : buffer) {//Iterate all characters
		if(inNameTag) {
			if(ch == ']') {
				//Don't do anything, tag stops at newline
			}
			else if(ch == '\n') {
				inNameTag = false;
			}
			else{
				nameTag += ch;
			}
		}
		else if(inElementContent) {
			if(ch == '\n' || ch == '#') {
				inElementContent = false;
				elements.insert(std::pair<std::string,std::string>(elementName,elementContent));
				elementName.clear();
				elementContent.clear();
				if(ch == '#') {
					inComment = true;
				}
			}
			else {
				elementContent += ch;
			}
		}
		else if(inComment) {
			if(ch == '\n') {
				inComment = false;
			}
		}
		else if(ch == '[') {
			inNameTag = true;
		}
		else if(ch == '=') {
			inElementContent = true;
		}
		else if(ch == '\n') {
			if(!nameTag.empty() || !elements.empty()) {
				lookup.insert(std::pair<std::string,std::map<std::string,std::string> >(nameTag,elements));
				nameTag.clear();
				elements.clear();
			}
		}
		else if(ch == '#') {
			inComment = true;
		}
		else {
			elementName += ch;
		}
	}
	if(!nameTag.empty() || !elements.empty()) {
		lookup.insert(std::pair<std::string,std::map<std::string,std::string> >(nameTag,elements));
		nameTag.clear();
		elements.clear();
	}
	return lookup;
}

const std::string PropertyFile::returnAsString(const std::string& find,
													const std::map<std::string,std::string>& table) {
	const auto found = table.find(find);
	if(found == table.end()) {
		std::string str = "Entry \"" + find + "\" not found";
		throw Framework::ParseException(str);
	}
	return found->second;
}

const int PropertyFile::returnAsInt(const std::string& find, const std::map<std::string,std::string>& table) {
	int i;
	try{
		i = std::stoi(returnAsString(find,table));
	}//end try
	catch(std::exception& e) {
		if(std::string(e.what()) == "stoi") {
			throw ParseException("Could not convert value to integer. Check your configuration files.");
		}
	}
	return i;
}

const double PropertyFile::returnAsDouble(const std::string& find, const std::map<std::string,std::string>& table) {
	double d;
	try{
		d = std::stod(returnAsString(find,table));
	}//end try
	catch(std::exception& e) {
		if(std::string(e.what()) == "stod") {
			throw ParseException("Could not convert value to floating point number. Check your configuration files.");
		}
	}
	return d;
}

const bool PropertyFile::returnAsBool(const std::string& find, const std::map<std::string,std::string>& table) {
	return toBool(returnAsString(find,table));
}

const bool PropertyFile::isEntryInLookup(const std::string& name, const Lookup& lookup) {
	const auto found = lookup.find(name);
	if(found != lookup.end()) {
		return true;
	}
	else {
		return false;
	}
}

const bool PropertyFile::toBool(std::string str) {
    std::transform(str.begin(), str.end(), str.begin(), ::tolower); //Make sure all characters are in lower case
    std::istringstream is(str);
    bool b;
    is >> std::boolalpha >> b; //With std::boolalpha a boolean can be inserted into b by its textual representation
    return b;
}

PropertyFile::~PropertyFile() {
	_file.close();
}


ParseException::ParseException(std::string m)  noexcept:
			Exception("ParseException", m) {

}

ParseException::~ParseException() noexcept{
}


} /* namespace framework */

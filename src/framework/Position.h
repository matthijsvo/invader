#ifndef POSITION_H_
#define POSITION_H_

/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include <math.h>

namespace Framework {

const double TAU = 6.2831853071;
const double DEG_TO_RAD = TAU/360.0;
const double RAD_TO_DEG = 360.0/TAU;

/**
 * @class	Position
 * @brief	Contains a position and rotation on an internal 2D plane
 */
class Position {
private:
	/**
	 * The internal resolution of the game. It is always square.
	 * Through Position it can be accessed by anything that needs it.
	 */
	static double _resolution; //Only largest side

public:
	static const double getResolution();

	static void setResolution(double resolution);

	Position(double x=0, double y=0, double rot=0);

	/**
	 * @brief	Compares the x and y coordinates of two Positions
	 */
	bool operator==(const Position& other);

	const double getX() const;

	const double getY() const;

	const double getRotation() const;

	/**
	 * @brief	Calculates the distance between the center of this Position to that of another
	 */
	const double getDistance(const Position& pos) const;

	/**
	 * @brief	Calculates the distance between the center of this Position and a pair of coordinates
	 */
	const double getDistance(double x, double y) const;

	/**
	 * @brief	Returns the angle from this Position to another
	 */
	const double getRotationTowards(const Position& pos);

	/**
	 * @brief	Returns the angle from this Position to a pair of coordinates
	 */
	const double getRotationTowards(double x, double y);

	virtual void setX(double x);

	virtual void setY(double y);

	void setPosition(double x, double y);

	void setRotation(double r);

	void addRotation(double r);

	/**
	 * @brief	Move this Position a certain distance, according to its rotation
	 */
	void move(double distance, double factor = 1);

	/**
	 * @brief	Move this Position a certain distance, according to a given rotation
	 *
	 * The 'follow' argument decides whether this Position should also change its own rotation to that which is given
	 */
	void moveInDirection(double rot, double distance, double factor = 1, bool follow = true);

	/**
	 * @brief	Move a certain distance towards a pair of coordinates
	 */
	void moveTowards(double x, double y, double distance);

	/**
	 * @brief	Move a certain distance towards another Position
	 */
	void moveTowards(const Position& pos, double distance);

	/**
	 * @brief	Rotate this Position so it points at another
	 */
	void rotateTowards(const Position& pos);

	/**
	 * @brief	Rotate this Position so it points at a certain pair of coordinates
	 */
	void rotateTowards(double x, double y);

	/**
	 * @brief	Rotate this Position a certain amount of degrees around a pair of coordinates while maintaining the
	 * 			same distance.
	 */
	void rotateAround(double x, double y, double theta);

	/**
	 * @brief	Returns if the Position lies outside the internal resolution (_resolution).
	 */
	bool outsideBounds();

	virtual ~Position();
private:
	/**
	 * @brief	"corrects" all angles to fit within 0 to 360 degrees
	 */
	const double correctRotation(double rot);
private:
	double _x;

	double _y;

	double _rot;
};

} /* namespace framework */

#endif /* POSITION_H_ */

#ifndef ABSTRACTSPRITECREATOR_H_
#define ABSTRACTSPRITECREATOR_H_

/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include "../framework/EventListener.h"
#include "EntitySprite.h"

namespace Visual {

/**
 * @class	AbstractSpriteCreator
 * @brief	Factory that creates sprites
 *
 * Abstract class, should be derived.
 */
class AbstractSpriteCreator {
public:
    typedef std::shared_ptr<AbstractSpriteCreator> sPtr;
    typedef std::weak_ptr<AbstractSpriteCreator> wPtr;

public:
    AbstractSpriteCreator();

	/**
	 * @brief	Create and return a sprite based on the name given
	 */
	virtual EntitySprite::sPtr createSprite(const std::string& name) = 0;

    virtual ~AbstractSpriteCreator();
};

} /* namespace Visual */

#endif /* ABSTRACTSPRITECREATOR_H_ */

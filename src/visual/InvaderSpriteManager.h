#ifndef INVADERSPRITEMANAGER_H_
#define INVADERSPRITEMANAGER_H_

/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include <SFML/Graphics.hpp>
#include <map>
#include "../framework/Entity.h"
#include "../framework/EventListener.h"
#include "EntitySprite.h"
#include "AbstractSpriteCreator.h"

namespace Visual {

/**
 * @class	InvaderSpriteManager
 * @brief	Manages sprites corresponding to entities
 *
 * The InvaderSpriteManager should be the sole maker and owner of its sprites. Creation happens through the
 * InvaderSpriteManager's own SpriteCreator.
 */
class InvaderSpriteManager : public Framework::CreationEventListener,
								public Framework::DeathEventListener {
public:
	typedef std::shared_ptr<InvaderSpriteManager> sPtr;
	typedef std::weak_ptr<InvaderSpriteManager> wPtr;
public:
	/**
	 * @brief	Creates an InvaderSpriteManager and returns a shared_ptr to it
	 *
	 * This constructor, following the Named Constructor Idiom, makes sure that the InvaderSpriteManager is created as
	 * usual, and also sets a pointer to itself (see "_this").
	 */
    static InvaderSpriteManager::sPtr make(AbstractSpriteCreator::wPtr spriteFactory);

	/**
	 * @brief	Draws all sprites on the window
	 */
	void drawAll();

	/**
	 * @brief	Resets the InvaderSpriteManager to its original state
	 *
	 * Deletes all contained sprites
	 */
	void clear();

	virtual ~InvaderSpriteManager();

private:
	/**
	 * @brief	Regular constructor for InvaderSpriteManager
	 *
	 * Receives the SpriteCreator that will be used to create new sprites when needed.
	 */
    InvaderSpriteManager(AbstractSpriteCreator::wPtr spriteFactory);

	void addSelfPtr(InvaderSpriteManager::wPtr self);

	/**
	 * @brief	Adds a new sprite based on the entity received
	 *
	 * Passes the necessary information to the SpriteCreator, which creates a new sprite.
	 */
	void add(Framework::Entity::wPtr entity);

	/**
	 * @brief	Removes the sprite of a certain entity
	 */
	void remove(Framework::Entity::wPtr entity);

	/**
	 * @brief	When a new entity is created, create a sprite for it
	 *
	 * This is the only way sprites should be created.
	 */
	void notifyCreation(Framework::Entity::wPtr entity);

	/**
	 * @brief	When an entity is flagged as "dead", remove its sprite
	 */
	void notifyDeath(Framework::Entity::wPtr entity);

private:
	/**
	 * Little POD for storing an entity and its sprite
	 */
	struct SpritePair {
		Framework::Entity::wPtr _entity;
		EntitySprite::sPtr _sprite;
		SpritePair(const Framework::Entity::wPtr& entity, EntitySprite::sPtr& sprite) :
			_entity(entity), _sprite(sprite) {
		}
	};

	/**
	 * Custom comparison functor for the container of this InvaderSpriteManager.
	 * Sorts sprites based on their layer (see EntitySprite::_layer). This makes sure the sprites are drawn in the
	 * correct order.
	 */
	struct sprite_comparison {
		bool operator()(const SpritePair& sp1, const SpritePair& sp2) {
			return (sp1._sprite->getLayer() <= sp2._sprite->getLayer());
		}
	};

private:
	/**
	 * Equivalent to the standard "this" pointer, but using a weak_ptr. Should be set when creating the
	 * InvaderSpriteManager with its "make" method.
	 * This pointer is meant to be used when the InvaderSpriteManager adds itself as a *Listener of another object.
	 */
	InvaderSpriteManager::wPtr _this;

	/**
	 * Contains all sprites, sorted by layer.
	 */
	std::set<SpritePair, sprite_comparison> _sprites;

	/**
	 * Creates all sprites for the InvaderSpriteManager.
	 */
    AbstractSpriteCreator::sPtr _spriteFactory;
};

/**
 * @class	SpriteManagerException
 * @brief	Used to report all exceptions that happen within an InvaderSpriteManager
 */
class SpriteManagerException : public Framework::Exception {
public:
	SpriteManagerException(std::string = "") noexcept;

	virtual ~SpriteManagerException() noexcept;
};

} /* namespace visual */

#endif /* INVADERSPRITEMANAGER_H_ */

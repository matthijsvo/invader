#ifndef ENTITYSPRITE_H_
#define ENTITYSPRITE_H_

/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include <memory>
#include "../framework/EventListener.h"
#include "../framework/Position.h"

namespace Visual {

/**
 * @class	EntitySprite
 * @brief 	The sprite of a single Entity
 *
 * Should be derived
 */
class EntitySprite : public Framework::PositionEventListener{
public:
	typedef std::shared_ptr<EntitySprite>sPtr;
	typedef std::weak_ptr<EntitySprite>wPtr;

	EntitySprite(int layer = 1);

	/**
	 * @brief	Draw this sprite on its window
	 */
	virtual void draw() = 0;

	const int getLayer() const;

	virtual void setToPosition(const Framework::Position& position) = 0;

	virtual ~EntitySprite();
protected:
	/**
	 * @brief	Notify when an observed object changes position.
	 *
	 * This is mainly intended for the Entity this Sprite is supposed to be drawing.
	 */
	virtual void notifyPosition(const Framework::Position& position) = 0;
private:
	/**
	 * The "layer" on which this sprite should be drawn on the window. Sprites with a higher layer number get drawn
	 * over those with a lower layer number.
	 */
	int _layer;
};

} /* namespace Visual */

#endif /* ENTITYSPRITE_H_ */

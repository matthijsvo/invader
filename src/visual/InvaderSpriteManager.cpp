/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include "InvaderSpriteManager.h"
#include <iostream>

namespace Visual {

InvaderSpriteManager::sPtr InvaderSpriteManager::make(AbstractSpriteCreator::wPtr spriteFactory) {
	InvaderSpriteManager* manager = new InvaderSpriteManager(spriteFactory);
	auto managerSPtr = std::shared_ptr<InvaderSpriteManager>(manager);
	manager->addSelfPtr(managerSPtr);
	return managerSPtr;
}

void InvaderSpriteManager::addSelfPtr(wPtr self) {
	_this = self;
}

InvaderSpriteManager::InvaderSpriteManager(AbstractSpriteCreator::wPtr spriteFactory) :
		_spriteFactory(spriteFactory) {

}

void InvaderSpriteManager::add(Framework::Entity::wPtr entity) {
	auto sprite = _spriteFactory->createSprite(entity.lock()->getName());
	sprite->setToPosition(entity.lock()->getPosition());
	entity.lock()->addPositionListener(sprite);
	_sprites.insert(SpritePair(entity, sprite));
}

void InvaderSpriteManager::remove(Framework::Entity::wPtr entity) {
	auto find = find_if(_sprites.begin(), _sprites.end(),
			[entity] (const SpritePair& pair) -> bool {return pair._entity.lock() == entity.lock();});

	if(find == _sprites.end()) {
		throw SpriteManagerException("Minor Error: Can't find the sprite \"" + entity.lock()->getName() +
				"\" that should be deleted.");
	}
	else {
		find->_entity.lock()->removePositionListener(find->_sprite); //Remove Observer connections first
		_sprites.erase(find);
	}
}

void InvaderSpriteManager::drawAll() {
	for(const auto& view : _sprites) {
		view._sprite->draw();
	}
}

void InvaderSpriteManager::clear() {
	_sprites.clear();
}

void InvaderSpriteManager::notifyCreation(Framework::Entity::wPtr entity) {
	add(entity);
}

void InvaderSpriteManager::notifyDeath(Framework::Entity::wPtr entity) {
	remove(entity);
}

InvaderSpriteManager::~InvaderSpriteManager() {
}

SpriteManagerException::SpriteManagerException(std::string m)  noexcept:
			Exception("SpriteManagementException", m) {

}

SpriteManagerException::~SpriteManagerException() noexcept{
}

} /* namespace visual */

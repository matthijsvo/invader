/**
 * @author  Matthijs Van Os - 20121014
 * @date    13/01/14
 * @version 1.0
 */

#include "EntitySprite.h"

namespace Visual {

EntitySprite::EntitySprite(int layer) : _layer(layer){
}

const int EntitySprite::getLayer() const {
	return _layer;
}

EntitySprite::~EntitySprite() {
}

} /* namespace Visual */

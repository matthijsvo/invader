################################################################################
#======================#/   Matthijs Van Os           \#=======================#
#   PROJECT:           #    Bac2 Informatica           #                       #
#           INVADER    #    Universiteit Antwerpen     #                       #
#======================#\   20121014                  /#=======================#
################################################################################


# PROJECT INVADER:
    Project: Invader is een spel gebaseerd op de klassieke Space Invaders.


# COMPILATIE:
    Het project dient gecompileerd te worden met CMake.
    Vereisten zijn: CMake 2.6 (of later), gcc 4.8 (of later) en de SFML 2.0 
    library (of later).

    Geef in deze map (invader) de commando's
    $ cmake .
    $ make
    en de executable 'invader' wordt aangemaakt in de map 'bin'


# HET MENU:
    In het menu is het mogelijk een level te kiezen.
    Deze levels zijn standaard ingedeeld in traditionele levels ('Level [X]') en
    experimentelere modi (aangeduid door 'experimental').
    Eveneens kan men zelf de naam van een level ingeven onder '[enter level]'.
    Het menu wordt bediend met de muis. Bij het zelf ingeven van een level dient
    men eerst de toets aan te klikken, vervolgens de naam van het level in te
    geven en dan op 'Return/Enter' te drukken. Het level wordt gezocht in de map 
    './bin/resources/levels', de in te geven naam is de bestandsnaam ZONDER de 
    '.ini' extensie.
    De laatste knop 'Quit' sluit, vanzelfsprekend, het programma af. De Escape
    toets indrukken doet hetzelfde.

    Het is mogelijk het menu aan te passen en zelf nieuwe toetsen voor levels 
    toe te voegen. (zie CUSTOMIZEN)


# HET SPEL:
    De elementen van een traditioneel* level zijn relatief simpel:

 ## HET SCHIP
    De speler bestuurt het schip onderaan het scherm met de pijltjestoetsen of
    toetsen 'A/Q' en 'D' (dit is de aangeraden methode). Deze roteren de speler
    rond de aarde, te zien onder de speler.
    Drukken op de spatiebalk schiet een kogel af. Accuraat mikken kan enige
    oefening vereisen: als de speler in beweging is lijkt de kogel af te buigen.
    In het begin kan het makkelijker zijn altijd even te stoppen vooraleer te 
    schieten.
    Het schip kan geraakt worden door kogels van naderende aliens (en de aliens 
    zelf). Als dit gebeurt verliest het schip 1 leven (te zien links bovenaan). 
    Als alle levens op zijn verliest de speler het spel.

 ## ALIENS
    Doorheen de levels naderen veel verschillende soorten aliens op 
    verschillende manieren de aarde. Allemaal kunnen ze neergeschoten worden
    met één of meerdere kogels of raketten.

 ## DE AARDE
    De aarde bevindt zich steeds onder de speler. Zij is enkel kwetsbaar voor de
    aliens zelf, kogels hebben geen invloed. Als te veel aliens de aarde
    bereiken en die vernietigen, is het spel eveneens verloren.

 ## SCHILDEN
    Vaak zijn tussen de het schip van de speler en de aliens enkele schilden
    aanwezig. Deze vangen kogels en aliens op maar gaan kapot na te veel schade.

 ## DE LANCEERBASIS
    In latere levels kunnen op de aarde één of meerdere raketbasissen aanwezig 
    zijn die geleide raketten kunnen afschieten. Een commando om te vuren wordt
    gegeven door een linkermuisklik. De raketten volgen de muisaanwijzer en
    wanneer deze bereikt wordt vliegen zij rechtdoor.
    Raketten exploderen in contact met aliens, maar ook met hun kogels.
    Wees zuinig op de raketten! Hun voorraad is gelimiteerd.

 ## POWERUPS
    Af en toe zal de speler hulp krijgen in de vorm van een powerup die naar
    beneden komt gevallen. De effecten hiervan kunnen variëren van het vergroten
    van de beweeglijkheid van de speler tot het krijgen van een extra kanon op
    het schip.

 ## DE MINIMAP
    Rechtsboven in het scherm is de minimap te zien. Deze geeft een overzicht
    over alles wat zich misschien buiten het zicht van de speler afspeelt.
    De schip wordt aangegeven door een gele stip, schilden en de aarde door
    blauwe stippen, vijanden door rode stippen en powerups door paarse stippen.
    Het is verstandig af en toe eens naar de minimap te kijken voor het geval
    aan de andere kant van de planeet een powerup naar beneden komt of een 
    nieuwe golf aliens is gearriveerd.

 ## OVERWINNING EN VERLIES
    Als de speler alle aliens vernietigd heeft, of juist verloren heeft door 
    het verliezen van zijn schip of de aarde, eindigt het spel. Het is mogelijk
    terug te keren naar de levelselectie door te drukken op de Escape toets.

 ## DE ACHTERGROND
    Als de achtergrond te veel afleidt van de actie op het scherm, is het
    mogelijk deze uit te zetten met de 'B' toets.

    *Experimentele of zelfgemaakte (zie CUSTOMIZEN) levels hoeven zich niet 
    noodzakelijk aan al deze regels te houden.


# CUSTOMIZEN:
    Voor wie de standaardselectie aan vijanden en levels beu is, is het mogelijk 
    om de bestaande entiteiten aan te passen of aan te vullen en zelf levels bij
    te maken.

    Voor alle door de gebruiker aan te passen bestanden wordt een syntax gelijk-
    aardig aan die van een standaard .ini bestand gehanteerd.
    Tussen vierkante haakjes staat de kop; dit kan de titel of de naam zijn.
    Onder de kop staan de attributen, met eerst de naam van het attribuut, dan
    een gelijkheidsteken, en dan een keuzeplaats waar een getal of string kan
    ingevuld worden (naargelang de context).
    Alles getypt na het commentsymbool '#' wordt genegeerd en kan gebuikt worden
    om commentaar bij te schrijven.

    Als algemene protip is het verstandig om een backup te maken van de
    standaard configuratiebestanden vooraleer je aanpassingen doet.

 ## MENU
    Het levelselectiemenu is aan te passen in het bestand 'menuProperties.ini'
    in de 'resources' map. De verschillende types van knoppen en hun attributen
    zijn af te leiden uit ditzelfde bestand zoals het standaard geleverd wordt.

 ## ENTITEITEN
    Alle entiteiten in het spel zijn aan te passen in het bestand 
    'entityProperties.ini' in de 'resources' map.
    Het is voor de gebruiker mogelijk zelf naar believen nieuwe entiteiten aan
    te maken, al blijven ze wel beperkt in hun gedrag tot een aantal basistypen.
    Als kop staat steeds de naam geschreven. Deze namen dienen ook gebruikt te
    worden bij het maken van een nieuw level (zie LEVELS).
    Onder de naam staan de verscheidene attributen, verschillend per gekozen
    type (eveneens een attribuut). Het originele document bevat alle types
    en kan gebruikt worden als voorbeeld.
    Enkele types attributen hebben echter misschien nog wat verduidelijking 
    nodig.
    Achter 'mini' hoort de (kleine) sprite die gebruikt wordt op de minimap.
    'layer' bepaalt op welke "laag" de entiteit getekend wordt. Entiteiten met
    een "hogere laag" zijn zichtbaar boven diegenen met een "lagere laag".
    Bij 'size' kan gewoon de hoogte van het afbeeldingsbestand ('sprite')
    ingegeven worden. Het wordt ten zeerste aangeraden steeds cirkelvormige of
    vierkante sprites te gebruiken, andere vormen worden momenteel niet correct 
    gedetecteerd.

 ## LEVELS
    In de 'levels' map kunnen levels aangepast worden of nieuwe bijgemaakt
    worden. De configuratie is redelijk simpel.
    Eerst worden onder de kop '[lives]' de entiteiten gegeven die in leven 
    moeten blijven. Dit houdt ook in dat deze entiteiten terug tot leven 
    gebracht worden tot hun levenscijfer op 0 staat. Een ingave van '-1' wordt 
    als oneindig geïnterpreteerd.
    Vervolgens worden onder de kop '[setup]' de entiteiten gespecifiëerd die
    aan het begin van het spel aanwezig moeten zijn op het speelveld.
    Onder de kop '[wave]' worden de golven van binnenkomende aliens of powerups
    aangegeven. Het 'time'-attribuut geeft de tijd (in seconden) aan vanaf de
    start van het level waarop de golf in het spel moet gebracht worden.
    Achter 'entityType' moet ingegeven worden welke entiteit aangemaakt moet
    worden. Hier dient de naam ingegeven te worden zoals die in de entiteiten-
    configuratie gespecifiëerd is (zie ENTITEITEN).
    'rotation' en 'spread' bepalen respectievelijk de hoek vanaf het middelpunt 
    van het speelveld (waar normaal de aarde staat) tot de plaats van de eerste 
    aan te maken entiteit, en de hoek waarover 'amount' entiteiten gespreid
    worden.


# ASSETS
    De achtergrond is een aangepaste versie van een gratis stockafbeelding van 
    FS-XXVII:
        http://fs-xxvii.deviantart.com/art/Free-Night-Sky-Stars-Stock-329063094
    Het gebruikte lettertype is 04B_03 (freeware):
        http://www.04.jp.org/
    Alle sprites zijn door mij gemaakt binnen het kader van dit project.

